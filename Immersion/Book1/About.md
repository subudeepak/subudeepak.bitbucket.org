# About

- - -

Immersion is a free to read weekly novel by me (Deepak Subramanian). You are welcome to pass your comments and critiques on the various chapter pages. I hope to release one chapter every Sunday. However, I can not always assure the release. 

## Synopsis

All of us experience different and exciting worlds when reading a book. Can such an experience be enhanced ? This question plagued the employees of Legacy Systems Inc. In this series, we follow these employees and their attempt to create this future. A team of authors are hired and a device using the very latest of technology is created. When their dreams come true, would it truly be worth it ? Venturing into the very depths of virtual reality and getting lost in the book are not the worst of things.

The future in this so called SciFi is not very far from the present and contains a lot of things that could possibly happen even before the completion of this book. While a certain amount of exagerration is to be expected from a fictinal book, I have aimed to keep it more realistic atleast in the initial part of the book.

[Small spoiler: Just to make it clear. This is not story where a device traps people into the a virtual world or such.]

## Commenting

Please comment at the [discus channel](https://disqus.com/home/channel/immersionthenovel/)

- - - 

## Table of Contents

+ Book 1
  - [Chapter 1: Immersion](/#!/immersion/1)
  - [Chapter 2: In deep waters, the Hippos will dive !!](/#!/immersion/2)
  
- - -


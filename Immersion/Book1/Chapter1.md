# Chapter 1: Immersion 
- - - 

The program was one of the most important landmarks of the century and I was part of it. I still remember the day when I got my offer from Legacy Systems Inc. Well, its not been that long. I joined right out of college and have been here for a couple years now. Ah ! there goes my calendar alert for my first meeting with Dr. Richard Cinters. He is the author I am supposed to meet up with. He graciously agreed to participate in the program and Rajesh has been very excited about it as well.&nbsp; 

Speak of the devil. There he was, with his smirk as laughed out loud looking at me as only friends can. Well, can I blame him ? I looked ridiculous with a full-hand shirt, a tie and a pant that was not a jean. I was in the immersion program and more closer to the R&amp;D team and that always showed in my dressing sense. My mother came by the other day and just stopped short of clearing my entire closet into the bin.&nbsp; 

Rajesh was dressed pretty well too, but as a immersion program coordination manager, that was pretty normal. Much older than me, he was in fact on the team that first interviewed me. One can perhaps describe his physique as short and stout but whomsoever dared to do so would be most unfortunate. I was so nervous when I joined in this humongous organization. &nbsp;One would have thought we would have a very formal relationship back in those days. Things changed so much since then. The immersion program picked up and my ideas were catapulted by Rajesh. That was when we started working a lot together. He arranged for many things to happen and today was the first fruit of his labor that I was to witness. The meeting with Dr. Richard Cinters.&nbsp; 

The meeting was at a very formal restaurant at the part of the city that I have never visited despite spending 5 years here. Well, there was nothing fun to do there anyway. Everything is so damn expensive. Though thinking back, I can afford many such luxuries with my current paycheck. Rajesh was excited as well. This was after all the first time he would be seeing Dr. Cinters in person. He kept talking about how he had managed to get Dr. Cinters excited about this project, all his maneuvering and how the game was ours to loose. I had heard this for the n<sup>th</sup> time now and was so much more engrossed with recalling my table manners. My staple food of cereals, burgers, fries and boiled stuff don't seem to require the use of so much cutlery as that was placed on my table. Why couldn't we meet at a coffee shop like normal people ?&nbsp;

Ah! and there he was. Dr. Cinters was perhaps as badly dressed as I would have been if not for my mother. The only positive was he wore a shirt with a tie. His hair was as unruly as an 8 year old boy's and he bore a smile of serenity. He was a tall man with whitening hair. Rajesh almost gaped at the appearance and I was so much in bliss as if I was having a reunion. The greetings went smoothly and he took his seat immediately crossing his legs. Rajesh started, 

"Dr. Cinters, it has been an honor.."

"Can you cut it with the Dr. Cinters already. I prefer Richard. I have agreed to it already. So, lets let go off the niceties and formulate how we would be proceeding. I take it this is Vyas." 

I was still recovering from the black man's openness. He took me by surprise. I was still searching for words. 

"You do know about the immersion program Dr. Cinters .. ummm... Richard. I am here to explain what I was actually hoping to achieve."

"Perfect. I would like that as well. So, lets get this straight. You want me to write a book for your device right. I can understand that. But what I do not understand is why you cannot adopt one of my earlier works. I can still work with you. In fact, we could look into transforming the fantasy series 'The torn empire'." 

I was already not liking where this was headed. I needed to start over. If I needed to look at torn empire, I would have. Those were such good books. My initial idea was to do something precisely that. But what would be the fun in doing that. Rajesh and the entire business team did not want to spend so much money on books that were already made to movies. I loved the idea of working with an author to make a complete immersion book as well. That would allow us to exploit as much as possible from these new devices. They were going to be launched in the near future and we needed something grand for the opening. Something just for this. Rajesh knew it was his domain to convince. 

"Richard, you know that is not going to work. Even with all your description in those books, they do not provide the richness of experience that we want. We also want to sell and while I am sure torn empire will garner interest, it is not the primary project the management will throw money at. We need something fresh just for this."

"Haha! Business folk. The reason I left R&amp;D to write books!. Vyas, I need to know it from you."

"How do I put it .. Richard, I am excited about breaking the frontiers of this technology. It need not be the world of the torn empire but it does need to be for this device. Made for precisely this reason. You see, we want to build models, personalities, characters and so much more. We already have the means. I have been seeing great improvements in our algorithms. While emotions are still not accurate, we are closer than ever. I need to see what we can do. Most of the other teams are all interested in games or movies. I know the line is so blurred between these three but immersion books need to be different. If we needed to adopt the torn empire, it would be great for a game but not so for a book."

"Vyas, I am still not able to understand what the difference is between an immersion book and a movie. Based on what you have said, they seem to be so closely related."

"I know, it is so much closer than you would think. The main difference is the minute details. A movie is regardless a much more simpler version adhering to the plot of the book. A book always manages to capture so much more such as sentiments, emotions, thoughts .. The world created needs to be shown regardless of the story line. In an immersion book, I want to go further and create the world to a great detail that one can truly be lost in the book. This requires so much more than a game because there is no mission to do this. No purpose except to understand this world and the story behind it."

"Vyas, you had me the minute this was a technology process. I would have done it had it been a book, a movie script or a game story line. When can I come and see the device ?"&nbsp; 
 

<!-- 
I was so relieved to hear that. Dr. Cinters had been a scientist working with Artificial Intelligence and most of his research papers are still central to our work. A decade ago, he left his position to pursue his writing interests and became good at that too. Though one has to say, his research paper writing skills have affected his fiction styles such that most of his fans come from the geek world.--> 
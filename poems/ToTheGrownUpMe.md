#To the grown up me

- - -

When I look up, with my chin to the sky,  
With passion and energy, my heart does cry,  
My mind and heart are now doing great,  
Dear grown up me how are you of late?  
 
Songs to the wind I can sing still,  
Odes to an urn I can enjoy still,  
The whole world still leaves me dazzled,  
Dear grown up me have these really changed?  
 
The swimming pool, I love to great lengths,  
I swim and swim to depths and lengths,  
Despite great long strides, I remain without teeter,  
Dear grown up me are you much better?  
 
The world so huge, I have not seen,  
Places so many, I wish to have been,  
In experiences many I want to be drenched,  
Dear grown up me has this desire been quenched?  
 
A firm of my own I fervently desire,  
To reach great heights burn emotions like fire,  
Work towards it, I do with dedication,  
Dear grown up me hope I have not attained satisfaction.  
 
Climb I shall for now and ever,  
Climb I must for now and ever,  
To the future I look with fear and anticipation,  
Dear grown up me do write a nice conclusion :)  
 
```
Deepak Subramanian
Started October 6, 2011
Completed October 10, 2011
``` 
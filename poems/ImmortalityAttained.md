#Immortality Attained

- - -

Immortality indeed, never promised,  
Immortality, the envy of emperors and thieves,  
Immortality, in its true chaos,  
Immortality, not some God's prerogative.  

Not one soul, discriminated,  
Made by man, and man upkept,  
Every man leaves a trace unerased,  
Immortality, almost a sin.  

We shan't be forgotten,  
For every character a byte,  
And bytes ever cheaper,  
We shan't be forgotten.  

And yet we fear the fear within,  
For life and soul shall part their ways,  
Whilst bytes chew into life, sacrilege !  
Ghosts of the immortals never forget.  

We beg to be forgotten,  
For not all memories pleasant,  
Remembrance itself a sin,  
The past destructs from within.  

Indeed, the spoken word may never come back,  
But with time, it may be forgotten,  
Though the written word on the internet,  
Remembered, learnt and used to serve more relevant words :)  

> Remember, whatever you write will live forever. Do not give more fodder to the virtual you for you do not own it anymore.

```
Deepak Subramanian
July 10 2014
```
#To Dennis Ritchie 

- - -

I thank you with all my heart  
The beauty of the mind so smart
I hope your vision I shall see  
The wonderful world that is C.  
 
Turing no prouder shall have been,  
Systems beyond we shan't have seen,  
A mighty world you did fix,  
The wonderful world that is Unix.  
 
In memory, references will live,  
For creations a creator's preservative,  
A programmer I mourn for thee,  
In bytes you shall live for eternity.  
 
```
Deepak Subramanian
October 14 2011
```
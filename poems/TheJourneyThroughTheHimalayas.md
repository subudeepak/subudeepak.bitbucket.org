#The journey through the himalayas

- - -

Surrounded by mountains I went,  
A river flowing right beneath,  
The wind giving its pleasant breeze,  
The mountain air with all its peace.  

Up the way as I went,  
Two rivers meet,  
United they flow  
From there beneath.  
 
Oh! That green blanket  
With the snowy top  
Where else would I see  
A sight of such majesty.  
 
Up as I go,  
There are midway streams  
Some furious falls  
Not near though.  
 
Oh! That beautiful sight  
Still before my eyes  
A sight of such Majesty  
A sight of such Ecstacy  
A sight of enlivenment  
A sight of the Himalayas!  

> This was my first poem written on experience. This poem describes the himalayas which I saw during the Badri-Kedar trip. The rivers which meet are the Mandakini and Alaknanda to form the Ganges.

``` 
-Deepak Subramanian
2003
```

#The City Underground

- - -

Moving stairways that lead you through,  
Signs plentiful to guide you through,  
Lights that shine, night or day,  
Through this city walk millions a day.  
 
No traffic lights to stop your dash,  
No speed limits to restrict your rush,  
No cars, no busses to honk around,  
And cool air much pure, does you surround.   
 
Food to eat, and clothes to buy,  
This little city a shopper's eye,  
So many smiles and laughters galore,  
This little city seldom a bore.  
 
Yet that man, who looks so old,  
I see his hands shivering in cold,  
For the city, his life does sustain,  
In return, his hands clean porcelain,  
 
That old lady, did no one care ?  
So frail indeed, with pure white hair,  
A table I see was a dirty sight,  
She turned it white, with all her might.  
 
The city underground, so calm and nice,  
So busy, and with little to fight,  
Everyone here is but of their own,  
And emotions within no one else could own.  
 
And calm as I leave the city underground,  
I hear its distinct cries to be found,  
Ah the sky ! I missed it glorious sight,  
And the world turned back to loud and bright.  
 
```
Deepak Subramanian
Place=>Suntec to City Hall (SG)
September 18,2012
```
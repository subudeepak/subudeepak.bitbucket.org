#Seeing what I see

- - -

I saw many a sight,  
Indeed before my eyes,  
And hence my smile so bright.  
 
I smiled at his delight,  
He had time to rejoice,  
I saw many a sight.  
 
The view afar, not trite,  
To see his life so nice,  
And hence my smile so bright.  
 
Through him I see the light,  
But this my mind denies,  
I saw many a sight.  
 
He feels his path is right,  
I watch over his rise,  
And hence my smile so bright.  
 
Fragment, I am, despite  
His mind, where mine abides,  
I see many a sight,  
And hence my smile so bright.

> This is a Villanelle in Iambic Trimeter.

``` 
Deepak Subramanian
9th October 2012
``` 

# Ah ! My grandfather

- - -

I hesitate over and again,  
For these words, I write with pain;  
They indeed carry immense significance,  
The end of an era and my reluctant countenance.  

I never recall his head with hair,  
The first recollection, he was fond of his chair;  
His pleasant smile, his hearty laugh,  
To have lost them is awfully tough.  

Back in the days when my world was white,  
When all my troubles were shorter than my height !  
His stories of myth dazzlingly more mythical,  
In my memories they remain colorful and ethereal.   

Almost a decade, afar I've been  
My every call, his vigor I've seen,  
His memory of my ramblings, none dare surmount,  
And with awe at his age, I subsequently recount.  

A smile and a laugh, and eye with a sheen,  
The words from his lips, pure and clean,  
With ardent persistence and independence,  
A young heart he lived, sans need for repentance.   

I knew and knew he loved me to great lengths,  
Spoken with ardent enthusiasm, his words' importance immense,  
A simple gift I gave, he cherished to the end,  
These memories, I can but reminisce; in my heart's pedestal, suspend.  

My words shall not suffice,  
Nor will the ink;  
For his existence to me, sublime,  
And recollections, ceaseless in time.  

Memories flow, like the tears in my eyes  
For they cannot see him that one last time  
I do not regret the end of his time  
Yet my absence, a loss of a lifetime.  

He was a man with a son denied;  
Yet a glorious life he was destined;  
Became the very symbol of a fulfilled life to be vied;  
For 'twas a deathbed with multiple generations beside;  

He was magnificent in life;  
And even more magnificent in death;  
I wish you a pleasant journey, my beloved grandfather;  
Your glorious life, a pleasant sunset.  

**Shri Ram Jai Ram Jai Jai Ram**  

. . *(Silence)* . .   

- - - 

>> Written to commemorate the death of my grandfather  

>> It must be noted that I have no recollection of him having spoken any words of slander or even remotely venomous. A great man when he lived, a great man when he died. I pay homage to my beloved grandfather.  

```
Deepak Subramanian
```
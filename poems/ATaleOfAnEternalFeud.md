# Singara Chennai: A tale of an eternal feud

- - -

Today indeed, another day,  
It moves along, like yesterday,  
Events occur, unfathomable yet normal,  
Like the death of a dozen newborn.  

Yet my hometown, I miss dearly,  
Ravaged and tortured for eternity,  
A lasting grudge with elements of water,  
She stands still, in all majesty.  

The rain forsook her, all these years,  
Rivers so ancient, lay deprived,  
Grudged the sea for its unquenchable thirst,  
And longed for every precious drop that made its way.  

Perhaps her demands were intense,  
Perhaps she did lack in diplomacy,  
All her water returned to her, sans conditions,  
Yet the methods, so vicious, like an everlasting grudge.  

She will wail again for all her people,  
Again indeed as water ravaged,  
Lifetimes of people can never have seen,  
All waters returned through tsunamis and floods.  

Yet, I am sure, she will stand again,  
Her proud face with a hint of melancholy,  
Her looks to the sea filled with disdain,  
And a sneer at the mountains, that deny her right.  
For her people, indeed made her honored,  
Her heart, motherly, and soul, regal.  

Hope you are well, Madras aka Chennai.  

> This poem was written following flooding of chennai after intense cyclones.

```
Deepak Subramanian
03 December 2015
```
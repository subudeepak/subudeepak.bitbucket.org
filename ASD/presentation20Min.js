/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var presentationSlides = [
    "Intro",
    "Agenda",
    "Scope",
    "JavaScript",
    "JSONP",
    "Rel_1",
    "Rel_2_1",
    "Rel_2_2",
    "Rel_2",//"Rel_FlowFox_1",
    "Rel_FlowFox_2",
    "Rel_FlowFox_3",//"Rel_FacetedApproach_1",//"Rel_FacetedApproach_2",
    "Rel_FacetedApproach_3",
    "ASD_1",
    "ASD_2_1",
    "ASD_2_2",//"ASD_2_3",
    "ASD_3",
    "ASD_4",
    "ASD_5",
    "ASD_8",
    "InFlow_1",
    "InFlow_example",
    "InFlow_example_2",
    "Future"
];
function raphaelPrototyping()
{               
};
raphaelPrototyping.prototype.baseX = 0;
raphaelPrototyping.prototype.baseY = 0;
raphaelPrototyping.prototype.objects = {};                    
raphaelPrototyping.prototype.animations = {};           
raphaelPrototyping.prototype.step = 0;
raphaelPrototyping.prototype.paper = null;
raphaelPrototyping.prototype.createRaphael = function(){
    console.log("Implement Abstract method");
};
var asd_split_1 = new raphaelPrototyping;
var jsonpRaphael = new raphaelPrototyping;
var secureMultiExecutionRaphael = new raphaelPrototyping;
var asd_split_9 = new raphaelPrototyping;
asd_split_1.createRaphael = function(canvasName)
{
    this.objects = {};
    this.animations = {};

    document.getElementById(canvasName).innerHTML = "";
    document.getElementById("ASD_Code").innerHTML = prettyPrintOne("var a = 2;");
//    document.getElementById("ASD_1_Explanation").innerHTML="Note: JavaScript uses pointers <br> The above figure represents that the variable has a value <br> Click on the graph for next step";
    var paper = Raphael(canvasName, 1024, 419);
    this.paper = paper;
    this.baseX = 212 + 100;
    this.baseY = 10;
    this.step = 0;

    this.objects["rect1"] = paper.circle(this.baseX + 15, this.baseY + 215, 15);//paper.rect(this.baseX, this.baseY + 200, 30, 30);
    this.objects.rect1.attr("fill", "#fff");
    this.objects.rect1.attr("stroke", "#fff");
    this.objects["textVarName"] = this.paper.text(this.baseX,this.baseY+230,"A");
    this.objects.textVarName.attr("fill","#fff");this.objects.textVarName.attr("font-size","20");
    
    this.objects["rectNormal"] = paper.rect(this.baseX + 200, this.baseY + 200, 30, 30);
    this.objects.rectNormal.attr("fill", "#fff");
    this.objects.rectNormal.attr("stroke", "#fff");

    this.objects["rectNormal2"] = paper.rect(this.baseX + 300, this.baseY + 150, 0, 0);
    this.objects.rectNormal2.attr("fill", "#fff");
    this.objects.rectNormal2.attr("stroke", "#fff");


    this.objects["rectGreenDummy"] = paper.rect(this.baseX + 200, this.baseY + 300, 0, 0);//paper.rect(baseX + 200 , baseY + 20, 30, 30);
    this.objects.rectGreenDummy.attr("fill", "#0f0");
    this.objects.rectGreenDummy.attr("stroke", "#fff");

    this.objects["rectRedSecret"] = paper.rect(this.baseX + 200, this.baseY + 300, 0, 0);
    this.objects.rectRedSecret.attr("fill", "#f00");
    this.objects.rectRedSecret.attr("stroke", "#fff");
   // var text1 = paper.text(baseX + 11, baseY + 205, "A");

    //var circle1 = paper.circle(212, 70, 30);
    //var anim = Raphael.animation({cx: 210, cy: 120}, 2e2);

    //this.animations["anim2"] = Raphael.animation({path: "M242 45L272 300"},2e2);
    //circle1.animate(anim.delay(3000));
    //rect1.animate(anim2.delay(3000));
    this.objects["pathToNormal2"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 300)+" "+(this.baseY + 165);
    this.objects["pathToNormal"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 215);
    this.objects["pathToRedSecret"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 300);
    this.objects["pathToGreenDummy"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 50);

    this.objects["lineFromVar"] = paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 215));
    //line_toNormal.attr("stroke", "#fff");
    this.objects["lineFromVar2"] = paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 30)+" "+(this.baseY + 215));

    this.objects.lineFromVar.attr("stroke","#0f0");
    this.objects.lineFromVar.attr("stroke-width", "2");
    this.objects.lineFromVar.attr("arrow-end","diamond-wide-long");
    this.objects.lineFromVar2.attr("stroke-width", "2");
    //line_toNormal.animate(this.animations.anim2.delay(3000));
    console.log(this.objects.lineFromVar);
};
asd_split_1.nextStep = function() {
    this.step ++;
    switch(this.step)
    {
        case 1:
            {
                document.getElementById("ASD_Code").innerHTML = prettyPrintOne("a = 3;");
//                document.getElementById("ASD_1_Explanation").innerHTML = "var a = newval; <br>A new value is created and the variable's previous location is dissociated.<br>";
                this.animations["rectanim1"] = Raphael.animation({width:30, height:30},2e2,"linear",function() {        
                    asd_split_1.objects.lineFromVar.attr("stroke","#fff");

                });
                this.objects.rectNormal2.animate(this.animations.rectanim1);
                break;
            }            
        case 2:
            {         
//                    document.getElementById("ASD_1_Explanation").innerHTML = "var a = newval; <br> The variable is now associated to the new memory location.";
                    asd_split_1.objects.lineFromVar.animate({path:asd_split_1.objects.pathToNormal2},2e2,"linear",function(){asd_split_1.objects.rectNormal.attr("fill", "#666"); asd_split_1.objects.lineFromVar.attr("stroke","#0f0");}
                            );
                break;  
            }
        case 3:
            {
                document.getElementById("ASD_Code").innerHTML = prettyPrintOne("a = secret;");
//                document.getElementById("ASD_1_Explanation").innerHTML = "var a = secretval; <br> A secret value is created. The secret should not be stored like a regular value";
                this.objects.rectRedSecret.animate(this.animations.rectanim1);                           
                break;
            }
        case 4:
            {
//                document.getElementById("ASD_1_Explanation").innerHTML = "var a = secretval; <br> Hence, we create another dummy value and associate this value to the variable";
                asd_split_1.objects.lineFromVar.animate({path:asd_split_1.objects.pathToRedSecret},2e2,"linear",function(){asd_split_1.objects.rectNormal2.attr("fill", "#666"); asd_split_1.objects.lineFromVar.attr("stroke","#f00");}
                            );
                break;
            }
        case 5:
            {
                asd_split_1.objects.rectGreenDummy.animate({x:this.baseX+200,y:this.baseY+20,width:30,height:30},2e2,"linear",function(){
                    asd_split_1.objects.lineFromVar2.animate({path:asd_split_1.objects.pathToGreenDummy},2e2,"linear",function(){asd_split_1.objects.lineFromVar2.attr("stroke","#0f0");asd_split_1.objects.lineFromVar2.attr("arrow-end","circle-wide-long");}
                            );
                });
                break;
            }
        case 6:
            {
//                document.getElementById("ASD_1_Explanation").innerHTML = "The access to the secret is restricted (yellow line). <br> The conditions and restrictions for access are illustrated as part of the address split design.";
                asd_split_1.objects.lineFromVar.animate({stroke:"#ff0"},2e2,"linear",function(){
                    asd_split_1.objects.lineFromVar.attr("stroke-dasharray","- ."); var dictText = asd_split_1.paper.text(asd_split_1.baseX+100,asd_split_1.baseY+300,"ASD");dictText.attr("fill","#ff0");dictText.attr("font-size","30");
                });
                break;
            }
        case 7:
            {
//                document.getElementById("ASD_1_Explanation").innerHTML = "You have run through this animation. Use your arrow keys or spacebar to continue"
            }
        default:
            {
                break;
            }
    }
};
jsonpRaphael.createRaphael = function(canvasName)
{
    this.objects = {};
    this.animations = {};
    this.baseX = 0;
    this.baseY = -23;
    console.log("a");
    document.getElementById(canvasName).innerHTML = "";
    //document.getElementById("ASD_1_Explanation").innerHTML="Note: JavaScript uses pointers <br> The above figure represents that the variable has a value <br> Click on the graph for next step";
    this.paper = Raphael(canvasName, 1024, 319);
    this.objects["textFunctionA"] = this.paper.text(jsonpRaphael.baseX+100,jsonpRaphael.baseY+100,"Function A\n{\n\t...\n}");
    this.objects.textFunctionA.attr("fill","#ff0");
    this.objects.textFunctionA.attr("font-size","30");
    this.objects.textFunctionA.attr("text-anchor","start");
    
    this.objects["textServer"] = this.paper.text(jsonpRaphael.baseX+600,jsonpRaphael.baseY+200,"Server");
    this.objects.textServer.attr("fill","#ff0");
    this.objects.textServer.attr("font-size","30");
    this.objects.textServer.attr("text-anchor","start");
    
    this.objects["textFunctionB"] = this.paper.text(jsonpRaphael.baseX+100,jsonpRaphael.baseY+270,"Function B\n{\n\t...\n}");
    this.objects.textFunctionB.attr("fill","#ff0");
    this.objects.textFunctionB.attr("font-size","30");
    this.objects.textFunctionB.attr("text-anchor","start");
    
    this.objects["lineCenter"] = this.paper.path("M"+(this.baseX+500)+" "+(this.baseY + 3)+"L"+(this.baseX+500)+" "+(this.baseY+400));
    this.objects.lineCenter.attr("stroke","#fff");
    
    this.objects["lineToServer"] = this.paper.path("M"+(this.baseX+150)+" "+(this.baseY + 130)+"L"+(this.baseX+150)+" "+(this.baseY+130));
    this.objects.lineToServer.attr("stroke","#0ff");
    this.objects.lineToServer.attr("stroke-width", "2");
    this.objects.lineToServer.attr({"arrow-end":"diamond-wide-long"});
    this.objects["lineFromServer"] = this.paper.path("M"+(this.baseX+600)+" "+(this.baseY + 200)+"L"+(this.baseX+600)+" "+(this.baseY+200));
    this.objects.lineFromServer.attr("stroke","#0ff");
    this.objects.lineFromServer.attr("stroke-width", "2");
    this.objects.lineFromServer.attr({"arrow-end":"diamond-wide-long"});
    
    this.objects.lineToServer.animate({path:"M"+(this.baseX+150)+" "+(this.baseY + 130)+"L"+(this.baseX+600)+" "+(this.baseY+200)},2e3,"linear",function(){
        jsonpRaphael.objects.lineFromServer.animate({path:"M"+(jsonpRaphael.baseX+600)+" "+(jsonpRaphael.baseY + 200)+"L"+(jsonpRaphael.baseX+150)+" "+(jsonpRaphael.baseY+300)},2e3,"linear");
    });  
    console.log("b");
};
secureMultiExecutionRaphael.createRaphael = function(canvasName) {
    
};
asd_split_9.createRaphael = function(canvasName) {
    this.objects = {};
    this.animations = {};

    document.getElementById(canvasName).innerHTML = "";
    document.getElementById("ASD_9_Explanation").innerHTML="A view of the stack <br> The above figure represents function calls in a stack <br> Click on the graph for next step";
    var paper = Raphael(canvasName, 1024, 419);
    this.paper = paper;
    this.baseX = 185 + 100;
    this.baseY = 10;
    this.step = 0;

    this.objects["stackLine"] = paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 405)+ "L"+(this.baseX + 30)+" "+(this.baseY));
    this.objects.stackLine.attr("stroke","#f79");
    this.objects.stackLine.attr("stroke-width", "2");
    this.objects.stackLine.attr("arrow-end","block-wide-long");
    
    this.objects["textStackLine"] = paper.text(this.baseX,this.baseY + 220,"Stack");
    this.objects.textStackLine.attr("fill","#f79");
    this.objects.textStackLine.attr("font-size","20");
    this.objects.textStackLine.rotate(-90);
    //First
    this.objects["circleSelfSufficientOuter"] = paper.circle(this.baseX + 30, this.baseY + 380, 13);
    this.objects.circleSelfSufficientOuter.attr("stroke","#f79");
    this.objects.circleSelfSufficientOuter.attr("stroke-width", "2");
    
    this.objects["circleSelfSufficientInner"] = paper.circle(this.baseX + 30, this.baseY + 380, 7);
    this.objects.circleSelfSufficientInner.attr("fill", "#fff");
    this.objects.circleSelfSufficientInner.attr("stroke","#f79");
    
    this.objects["textSelfSufficient"] = paper.text(this.baseX + 100,this.baseY + 380,"Function 1");
    this.objects.textSelfSufficient.attr("fill","#fff");
    this.objects.textSelfSufficient.attr("font-size","20");
    
    this.objects["circleExHandler1Outer"] = paper.circle(this.baseX + 30, this.baseY + 380, 13);
    this.objects.circleExHandler1Outer.attr("stroke","#f79");
    this.objects.circleExHandler1Outer.attr("fill", "#fff");
    this.objects.circleExHandler1Outer.attr("stroke-width", "2");
    
    this.objects["circleExHandler1Inner"] = paper.circle(this.baseX + 30, this.baseY + 380, 7);
    this.objects.circleExHandler1Inner.attr("fill", "#f79");
    this.objects.circleExHandler1Inner.attr("stroke","#f79");
    
    this.objects["textExHandler1"] = paper.text(this.baseX + 100,this.baseY + 380,"Exception\nInterceptor");
    this.objects.textExHandler1.attr("fill","#f79");
    this.objects.textExHandler1.attr("font-size","20");
    this.objects.textExHandler1.hide();this.objects.circleExHandler1Inner.hide();this.objects.circleExHandler1Outer.hide();
    //Second
    this.objects["circleUtility1Outer"] = paper.circle(this.baseX + 30, this.baseY + 300, 13);
    this.objects.circleUtility1Outer.attr("stroke","#f79");
    this.objects.circleUtility1Outer.attr("stroke-width", "2");
    
    this.objects["circleUtility1Inner"] = paper.circle(this.baseX + 30, this.baseY + 300, 7);
    this.objects.circleUtility1Inner.attr("fill", "#fff");
    this.objects.circleUtility1Inner.attr("stroke","#f79");
    
    this.objects["textUtility1"] = paper.text(this.baseX + 100,this.baseY + 300,"Function 2");
    this.objects.textUtility1.attr("fill","#fff");
    this.objects.textUtility1.attr("font-size","20");
    //Third
    this.objects["circleUtility2Outer"] = paper.circle(this.baseX + 30, this.baseY + 220, 13);
    this.objects.circleUtility2Outer.attr("stroke","#f79");
    this.objects.circleUtility2Outer.attr("stroke-width", "2");
    
    this.objects["circleUtility2Inner"] = paper.circle(this.baseX + 30, this.baseY + 220, 7);
    this.objects.circleUtility2Inner.attr("fill", "#fff");
    this.objects.circleUtility2Inner.attr("stroke","#f79");
    
    this.objects["textUtility2"] = paper.text(this.baseX + 100,this.baseY + 220,"Function 3");
    this.objects.textUtility2.attr("fill","#fff");
    this.objects.textUtility2.attr("font-size","20");
    
    //Fourth
    this.objects["circleSelfSufficientOuter2"] = paper.circle(this.baseX + 30, this.baseY + 140, 13);
    this.objects.circleSelfSufficientOuter2.attr("stroke","#f79");
    this.objects.circleSelfSufficientOuter2.attr("stroke-width", "2");
    
    this.objects["circleSelfSufficientInner2"] = paper.circle(this.baseX + 30, this.baseY + 140, 7);
    this.objects.circleSelfSufficientInner2.attr("fill", "#fff");
    this.objects.circleSelfSufficientInner2.attr("stroke","#f79");
    
    this.objects["textSelfSufficient2"] = paper.text(this.baseX + 100,this.baseY + 140,"Function 4");
    this.objects.textSelfSufficient2.attr("fill","#fff");
    this.objects.textSelfSufficient2.attr("font-size","20");
    
    this.objects["circleExHandler2Outer"] = paper.circle(this.baseX + 30, this.baseY + 160, 13);
    this.objects.circleExHandler2Outer.attr("stroke","#f79");
    this.objects.circleExHandler2Outer.attr("fill", "#fff");
    this.objects.circleExHandler2Outer.attr("stroke-width", "2");
    
    this.objects["circleExHandler2Inner"] = paper.circle(this.baseX + 30, this.baseY + 160, 7);
    this.objects.circleExHandler2Inner.attr("fill", "#f79");
    this.objects.circleExHandler2Inner.attr("stroke","#f79");
    
    this.objects["textExHandler2"] = paper.text(this.baseX + 100,this.baseY + 160,"Exception\nInterceptor");
    this.objects.textExHandler2.attr("fill","#f79");
    this.objects.textExHandler2.attr("font-size","20");
    this.objects.textExHandler2.hide();this.objects.circleExHandler2Inner.hide();this.objects.circleExHandler2Outer.hide();
    //Fifth
    this.objects["circleUtility3Outer"] = paper.circle(this.baseX + 30, this.baseY + 60, 13);
    this.objects.circleUtility3Outer.attr("stroke","#f79");
    this.objects.circleUtility3Outer.attr("stroke-width", "2");
    
    this.objects["circleUtility3Inner"] = paper.circle(this.baseX + 30, this.baseY + 60, 7);
    this.objects.circleUtility3Inner.attr("fill", "#fff");
    this.objects.circleUtility3Inner.attr("stroke","#f79");
    
    this.objects["textUtility3"] = paper.text(this.baseX + 100,this.baseY + 60,"Function 5");
    this.objects.textUtility3.attr("fill","#fff");
    this.objects.textUtility3.attr("font-size","20");
    
    //Dict 1
    this.objects["textDict1"] = paper.text(this.baseX + 460,this.baseY + 380,"Dictionary 1");
    this.objects.textDict1.attr("fill","#0f0");
    this.objects.textDict1.attr("font-size","20");
    this.objects.textDict1.hide();
    this.objects["textDict2"] = paper.text(this.baseX + 460,this.baseY + 140,"Dictionary 2");
    this.objects.textDict2.attr("fill","#0f0");
    this.objects.textDict2.attr("font-size","20");
    this.objects.textDict2.hide();
    this.objects["lineDict2Inner"] = this.paper.path("M"+(this.baseX + 400)+" "+(this.baseY + 120)+ "L"+(this.baseX + 400)+" "+(this.baseY + 160));
    this.objects.lineDict2Inner.attr("stroke","#0f0");
    this.objects.lineDict2Inner.attr("stroke-width", "2");
    this.objects["lineDict1Inner"] = this.paper.path("M"+(this.baseX + 400)+" "+(this.baseY + 360)+ "L"+(this.baseX + 400)+" "+(this.baseY + 400));
    this.objects.lineDict1Inner.attr("stroke","#0f0");
    this.objects.lineDict1Inner.attr("stroke-width", "2");
    this.objects.lineDict1Inner.hide();this.objects.lineDict2Inner.hide();
    
    this.objects["lineExceptionH"] = this.paper.path("M"+(this.baseX + 17)+" "+(this.baseY + 60)+ "L"+(this.baseX )+" "+(this.baseY + 60));
    this.objects.lineExceptionH.attr("stroke","#f40");
    this.objects.lineExceptionH.attr("stroke-width", "2");
    this.objects["lineExceptionV"] = this.paper.path("M"+(this.baseX - 30)+" "+(this.baseY + 60)+ "L"+(this.baseX - 30)+" "+(this.baseY + 60));
    this.objects.lineExceptionV.attr("stroke","#f40");
    this.objects.lineExceptionV.attr("stroke-width", "2");
    this.objects["textException"] = paper.text(this.baseX - 30,this.baseY + 60,"Exception");
    this.objects.textException.attr("fill","#0f0");
    this.objects.textException.attr("font-size","15");
    this.objects.lineExceptionH.hide();this.objects.lineExceptionV.hide();this.objects.textException.hide();
    
//    this.objects.circleSelfSufficientOuter.attr("stroke-width", "2");
/*
    this.objects["rect1"] = paper.rect(this.baseX, this.baseY + 200, 30, 30);
    this.objects.rect1.attr("fill", "#fff");
    this.objects.rect1.attr("stroke", "#fff");

    this.objects["rectNormal"] = paper.rect(this.baseX + 200, this.baseY + 200, 30, 30);
    this.objects.rectNormal.attr("fill", "#fff");
    this.objects.rectNormal.attr("stroke", "#fff");

    this.objects["rectNormal2"] = paper.rect(this.baseX + 300, this.baseY + 150, 0, 0);
    this.objects.rectNormal2.attr("fill", "#fff");
    this.objects.rectNormal2.attr("stroke", "#fff");


    this.objects["rectGreenDummy"] = paper.rect(this.baseX + 200, this.baseY + 300, 0, 0);//paper.rect(baseX + 200 , baseY + 20, 30, 30);
    this.objects.rectGreenDummy.attr("fill", "#0f0");
    this.objects.rectGreenDummy.attr("stroke", "#fff");

    this.objects["rectRedSecret"] = paper.rect(this.baseX + 200, this.baseY + 300, 0, 0);
    this.objects.rectRedSecret.attr("fill", "#f00");
    this.objects.rectRedSecret.attr("stroke", "#fff");
    
   // var text1 = paper.text(baseX + 11, baseY + 205, "A");

    //var circle1 = paper.circle(212, 70, 30);
    //var anim = Raphael.animation({cx: 210, cy: 120}, 2e2);

    //this.animations["anim2"] = Raphael.animation({path: "M242 45L272 300"},2e2);
    //circle1.animate(anim.delay(3000));
    //rect1.animate(anim2.delay(3000));
    
    this.objects["pathToNormal2"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 300)+" "+(this.baseY + 165);
    this.objects["pathToNormal"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 215);
    this.objects["pathToRedSecret"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 300);
    this.objects["pathToGreenDummy"] = "M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 50);

    this.objects["lineFromVar"] = paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 200)+" "+(this.baseY + 215));
    //line_toNormal.attr("stroke", "#fff");
    this.objects["lineFromVar2"] = paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 215)+ "L"+(this.baseX + 30)+" "+(this.baseY + 215));

    this.objects.lineFromVar.attr("stroke","#0f0");
    this.objects.lineFromVar.attr("stroke-width", "2");
    this.objects.lineFromVar.attr("arrow-end","diamond-wide-long");
    this.objects.lineFromVar2.attr("stroke-width", "2");
    //line_toNormal.animate(this.animations.anim2.delay(3000));
    console.log(this.objects.lineFromVar);*/
};
asd_split_9.nextStep = function() {
    this.step ++;
    switch(this.step)
    {
        case 1:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "Consider functions 1,4 are Self-sufficient and others are utility functions<br>";
                this.objects.textSelfSufficient.attr("text","Self Sufficient 1");//animate(this.animations.changeText);
                this.objects.textSelfSufficient.attr("x",this.baseX + 120);
                this.objects.textSelfSufficient2.attr("text","Self Sufficient 2");//animate(this.animations.changeText);
                this.objects.textSelfSufficient2.attr("x",this.baseX + 120);
                this.objects.textUtility1.attr("text","Utility 1");
                this.objects.textUtility1.attr("x",this.baseX + 83);
                this.objects.textUtility2.attr("text","Utility 2");
                this.objects.textUtility2.attr("x",this.baseX + 83);
                this.objects.textUtility3.attr("text","Utility 3");
                this.objects.textUtility3.attr("x",this.baseX + 83);
                break;
            }            
        case 2:
            {         
                    document.getElementById("ASD_9_Explanation").innerHTML = "Self-sufficient functions are associated to dictionaries.<br>";
                    this.objects["lineDict1"] = this.paper.path("M"+(this.baseX + 220)+" "+(this.baseY + 380)+ "L"+(this.baseX + 220)+" "+(this.baseY + 380));
                   this.objects.lineDict1.attr("stroke","#0f0");
                    this.objects.lineDict1.animate({path:"M"+(this.baseX + 200)+" "+(this.baseY + 380)+ "L"+(this.baseX + 400)+" "+(this.baseY + 380)},2e2,"linear",function(){//this.objects.rectNormal.attr("fill", "#666"); //asd_split_1.objects.lineFromVar.attr("stroke","#0f0");
                        asd_split_9.objects.textDict1.show();
                    });
                    this.objects["lineDict2"] = this.paper.path("M"+(this.baseX + 220)+" "+(this.baseY + 140)+ "L"+(this.baseX + 220)+" "+(this.baseY + 140));
                   this.objects.lineDict2.attr("stroke","#0f0");
                    this.objects.lineDict2.animate({path:"M"+(this.baseX + 200)+" "+(this.baseY + 140)+ "L"+(this.baseX + 400)+" "+(this.baseY + 140)},2e2,"linear",function(){//this.objects.rectNormal.attr("fill", "#666"); //asd_split_1.objects.lineFromVar.attr("stroke","#0f0");
                        asd_split_9.objects.lineDict1Inner.show();asd_split_9.objects.lineDict2Inner.show();
                        asd_split_9.objects.textDict2.show();
                    });
                break;  
            }
        case 3:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "The dictionaries are propagated down the stack. <br>";
                this.objects["lineStackDict1"] = this.paper.path("M"+(this.baseX + 200)+" "+(this.baseY + 380)+ "L"+(this.baseX + 200)+" "+(this.baseY + 380));
                this.objects.lineStackDict1.attr("stroke","#0f0");
                this.objects.lineStackDict1.attr("arrow-end","classic-wide-long");
                this.objects.lineStackDict1.animate({path:"M"+(this.baseX + 200)+" "+(this.baseY + 380)+ "L"+(this.baseX + 200)+" "+(this.baseY + 220)},2e2,"linear",function(){//this.objects.rectNormal.attr("fill", "#666"); //asd_split_1.objects.lineFromVar.attr("stroke","#0f0");
                        //asd_split_9.objects.textDict1.show();
                    });
                this.objects["lineStackDict2"] = this.paper.path("M"+(this.baseX + 200)+" "+(this.baseY + 140)+ "L"+(this.baseX + 200)+" "+(this.baseY + 140));
                this.objects.lineStackDict2.attr("stroke","#0f0");
                this.objects.lineStackDict2.attr("arrow-end","classic-wide-long");
                this.objects.lineStackDict2.animate({path:"M"+(this.baseX + 200)+" "+(this.baseY + 140)+ "L"+(this.baseX + 200)+" "+(this.baseY + 60)},2e2,"linear",function(){//this.objects.rectNormal.attr("fill", "#666"); //asd_split_1.objects.lineFromVar.attr("stroke","#0f0");
                        //asd_split_9.objects.textDict1.show();
                    });
                break;
            }
        case 4:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "However, there can be no propagation when the function call is to a self-sufficient function.<br>";
                this.objects["lineStackSeparation"] = this.paper.path("M"+(this.baseX + 30)+" "+(this.baseY + 200)+ "L"+(this.baseX + 30)+" "+(this.baseY + 200));
                this.objects.lineStackSeparation.attr("stroke","#f79");
                this.objects.lineStackSeparation.attr("stroke-width","2");
                this.objects.lineStackSeparation.attr("stroke-dasharray","--..");
                this.objects.lineStackSeparation.animate({path:"M"+(this.baseX + 30)+" "+(this.baseY + 200)+ "L"+(this.baseX + 500)+" "+(this.baseY + 200)},2e2,"linear",function(){//this.objects.rectNormal.attr("fill", "#666"); //asd_split_1.objects.lineFromVar.attr("stroke","#0f0");
                        //asd_split_9.objects.textDict1.show();
                    });
                break;
            }
        case 5:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "To ensure proper separation, exception interceptors are also added by the mechanism.<br/>";
                this.objects.circleSelfSufficientInner.animate({cy:this.objects.circleSelfSufficientInner.attr("cy")-50},3e2,"elastic",function(){});
                this.objects.circleSelfSufficientOuter.animate({cy:this.objects.circleSelfSufficientOuter.attr("cy")-50},3e2,"elastic",function(){});
                this.objects.circleSelfSufficientInner2.animate({cy:this.objects.circleSelfSufficientInner2.attr("cy")-30},3e2,"elastic",function(){});
                this.objects.circleSelfSufficientOuter2.animate({cy:this.objects.circleSelfSufficientOuter2.attr("cy")-30},3e2,"elastic",function(){});
                this.objects.circleUtility1Inner.animate({cy:this.objects.circleUtility1Inner.attr("cy")-25},3e2,"elastic",function(){});
                this.objects.circleUtility1Outer.animate({cy:this.objects.circleUtility1Outer.attr("cy")-25},3e2,"elastic",function(){});
                this.objects.textSelfSufficient.animate({y:this.objects.textSelfSufficient.attr("y")-50},3e2,"elastic",function(){});
                this.objects.textSelfSufficient2.animate({y:this.objects.textSelfSufficient2.attr("y")-30},3e2,"elastic",function(){});
                this.objects.textUtility1.animate({y:this.objects.textUtility1.attr("y")-25},3e2,"elastic",function(){});
                asd_split_9.objects.textExHandler2.show();asd_split_9.objects.circleExHandler2Inner.show();asd_split_9.objects.circleExHandler2Outer.show();
                asd_split_9.objects.textExHandler1.show();asd_split_9.objects.circleExHandler1Inner.show();asd_split_9.objects.circleExHandler1Outer.show();
                
                break;
            }
        case 6:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = 'Consider an exception is thrown by the function "utility 3"';
                this.objects.lineExceptionH.show();this.objects.textException.show();
                break;
            }
        case 7:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "Consider the function \"self-sufficient 2\" fails to handle the exception.";
                this.objects.textException.animate({y:this.objects.textException.attr("y")+50},2e2,"linear",function(){});
                this.objects.lineExceptionV.show();
                this.objects.lineExceptionV.animate({path:"M"+(this.baseX - 30)+" "+(this.baseY + 60)+ "L"+(this.baseX - 30)+" "+(this.baseY + 100)},2e2,"linear",function(){});
                this.objects.lineExceptionH.attr("path","M"+(this.baseX + 17)+" "+(this.baseY + 60)+ "L"+(this.baseX - 30 )+" "+(this.baseY + 60));
                this.objects.lineExceptionH.attr("stroke-linejoin","round");
                this.objects.lineExceptionV.attr("arrow-end","diamond-wide-long");
                this.objects.circleUtility3Inner.attr("stroke","#A9A9A9");
                this.objects.circleUtility3Inner.attr("fill","#A9A9A9");
                this.objects.circleUtility3Outer.attr("stroke","#A9A9A9");
                this.objects.textUtility3.attr("fill","#A9A9A9");
                this.objects.stackLine.animate({path:"M"+(this.baseX + 30)+" "+(this.baseY + 405)+ "L"+(this.baseX + 30)+" "+(this.baseY + 80)}, 2e2, "linear",function(){});
                
                break;
            }
        case 8:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "Such an exception is handled by the \"Exception Interceptor\" and the program continues without knowing that an exception has occurred.";
                this.objects.textException.animate({y:this.objects.textException.attr("y")+50},2e2,"linear",function(){});
                this.objects.lineExceptionV.animate({path:"M"+(this.baseX - 30)+" "+(this.baseY + 60)+ "L"+(this.baseX - 30)+" "+(this.baseY + 150)},2e2,"linear",function(){});
                
                this.objects.circleSelfSufficientInner2.attr("stroke","#A9A9A9");
                this.objects.circleSelfSufficientInner2.attr("fill","#A9A9A9");
                this.objects.circleSelfSufficientOuter2.attr("stroke","#A9A9A9");
                this.objects.textSelfSufficient2.attr("fill","#A9A9A9");
                this.objects.stackLine.animate({path:"M"+(this.baseX + 30)+" "+(this.baseY + 405)+ "L"+(this.baseX + 30)+" "+(this.baseY + 130)}, 2e2, "linear",function(){});
                break;
            }
        case 9:
            {
                document.getElementById("ASD_9_Explanation").innerHTML = "The seperation of the dictionary propagation is removed.<br/>You have run through this animation. Use your arrow keys or spacebar to continue";
                this.objects.textException.attr("fill","#A9A9A9");
                this.objects.lineExceptionV.attr("stroke","#A9A9A9");
                this.objects.lineExceptionH.attr("stroke","#A9A9A9");
                this.objects.lineDict2.attr("stroke","#A9A9A9");
                this.objects.lineDict2Inner.attr("stroke","#A9A9A9");
                this.objects.lineStackDict2.attr("stroke","#A9A9A9");
                this.objects.textDict2.attr("fill","#A9A9A9");
                this.objects.circleExHandler2Inner.attr("stroke","#A9A9A9");
                this.objects.circleExHandler2Inner.attr("fill","#A9A9A9");
                this.objects.circleExHandler2Outer.attr("stroke","#A9A9A9");
                this.objects.circleExHandler2Outer.attr("fill","#A9A9A9");
                
                this.objects.textExHandler2.attr("fill","#A9A9A9");
                this.objects.lineStackSeparation.hide();
                this.objects.stackLine.animate({path:"M"+(this.baseX + 30)+" "+(this.baseY + 405)+ "L"+(this.baseX + 30)+" "+(this.baseY + 190)}, 2e2, "linear",function(){});
                break;
            }
        default:
            {
                break;
            }
    }
};

$(function() 
    {
        $('#jmpress').jmpress({
            stepSelector:'.step',
                     setActive: function( element, eventData ){
                         var pgNo = (presentationSlides.indexOf(element[0].id)+1);
                         document.getElementById("pageNumber").innerHTML =  (pgNo > 0)? pgNo + " of " + presentationSlides.length : "";
                         switch(element[0].id)//invisible
                         {
                             case "JSONP_2":
                            case "Agenda":
                            case "Rel_1":
                             case "Rel_FlowFox_2":
                            case "Rel_FlowFox_3":
                            case "Rel_FacetedApproach_2":
                            case "Rel_FacetedApproach_3":
                            case "Rel_staged_2":
                            case "Rel_staged_3":
                            case "Rel_Probabilistic_2":
                            case "Rel_Probabilistic_3":
                            case "Rel_Probabilistic_5":
                            case "Rel_Others":
                            case "ASD_2":
                            case "ASD_1":
                            case "ASD_2_1":
                            case "ASD_2_2":
                            case "ASD_2_3":
                            case "InFlow_example_2":
                            case "Opinions":
                                element[0].classList.remove('invisible');
                                break;
                         }
                         switch(element[0].id)
                         {
                            case "Slide":
                                console.log('Slide1');
                                break;
                            case "Intro":
                                var introMask = document.getElementById('Intro_mask');
                                var introH2 = document.getElementById('Intro_h2');
                                var introP = document.getElementById('Intro_p');
                                var introA = document.querySelectorAll('a.info');
                                introMask.classList.add('mask_open');
                                introH2.classList.add('h2_open');
                                introP.classList.add('p_open');
                                for(var i = 0; i < introA.length; i++)
                                {
                                    introA[i].classList.add('info_open');
                                }
                                break;
                            case "ASD_1":                                            
                                asd_split_1.createRaphael("ASD_1_canvas");
                                $("#jmpress").jmpress("route", ["#Rel_FacetedApproach_3", "#ASD_1"]);
                                break;
                            case "ASD_9":
                                asd_split_9.createRaphael("ASD_9_canvas");
                                break;
                            case "JSONP":
                                jsonpRaphael.createRaphael("jsonp_canvas");
                            break;
                            
                            case "Rel_FlowFox_3":
                                $("#jmpress").jmpress("route", ["#Rel_2_2", "#Rel_2", "#Rel_FlowFox_2"]);
                                secureMultiExecutionRaphael.createRaphael("Rel_FlowFox_3_Canvas");
                                break;
                                
                            case "Rel_FacetedApproach_3":
                            case "Rel_staged_1":
                                $("#jmpress").jmpress("route", ["#Rel_FacetedApproach_3", "#Rel_2", "#ASD_1"]);
                            break; 
                            case "Rel_staged_2":
                                $("#jmpress").jmpress("route", ["#Rel_staged_2", "#Rel_2", "#Rel_Probabilistic_1"]);
                                break;
                            case "Rel_2_2":
                                $("#jmpress").jmpress("route", ["#Rel_2_2", "#Rel_2", "#Rel_FlowFox_2"]);
                            break;
                            case "Thanks":
                                element[0].innerHTML = "Thank You!";//<br><div class='normalHeading mildOpacity'>This presentation was made with JS</div>
                                break;
//                            case "Pbox": //incomplete
//                                try{
//                                document.getElementById('sub').removeEventListener('click',
//                                                            clickJacking);
//                                                        }
//                                catch (e) {
//                                                            
//                                                        }
//                                document.getElementById('sub').addEventListener('click',
//                                                            clickJacking);
//                                break;
                            default:
                                break;
                         }

                     },
                    setInactive:  function( element, eventData ){
                         switch(element[0].id)
                         {
                             case "Intro":
                                 var introMask = document.getElementById('Intro_mask');
                                 var introH2 = document.getElementById('Intro_h2');
                                 var introP = document.getElementById('Intro_p');
                                 var introA = document.querySelectorAll('a.info');
                                 introMask.classList.remove('mask_open');
                                 introH2.classList.remove('h2_open');
                                 introP.classList.remove('p_open');
                                 for(var i = 0; i < introA.length; i++)
                                 {
                                     introA[i].classList.remove('info_open');
                                 }
                                 break;
                            case "JSONP_2":
                            case "Agenda":
                            case "Rel_1":
                             
                            case "Rel_FlowFox_2":
                            case "Rel_FlowFox_3":
                            
                            case "Rel_FacetedApproach_2":
                            case "Rel_FacetedApproach_3":
                            case "Rel_staged_2":
                            case "Rel_staged_3":
                                
                            case "Rel_Probabilistic_2":
                            case "Rel_Probabilistic_3":
                            case "Rel_Probabilistic_5":
                            case "Rel_Others":
                            case "ASD_2":
                            case "ASD_1":
                            case "ASD_2_1":
                            case "ASD_2_2":
                            case "ASD_2_3":
                            case "InFlow_example_2":
                            case "Opinions":
                                element[0].classList.add('invisible');
                                break;
                            
                            case "Thanks":
                                element[0].innerHTML = "";
                                break;
                             default:
                                 break;
                         }
                    }
                 }
                );                   
    });                         
var codeStepInitial = 2;
var codeStep = 1;
var clickJacking = function(e)
                                                                    {
                                                                        alert(document.getElementById('pass').value);
                                                                        e.preventDefault();
                                                                    };
function nextCodeStep()
{
   codeStep ++;
   codeStepInitial += .9;
   if(codeStepInitial < 12)
   { 
        var codeSelector = document.getElementById("codeSelector");
        codeSelector.style.marginTop = codeStepInitial+"em";
   }
   switch(codeStep)
   {
       case 2:
           break;
       case 3:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>f1 has complete access to object a. </li><li>b's access rights are added to a.name.</li><li> So now f3 has access to a.name</li>";
           var tablef1 = document.getElementById("f1");
           var tr = document.createElement("tr");
           tr.id = 'f1_aname';
           tr.innerHTML = "<td>a.name</td><td>FB</td><td>0xbbbb</td>";
           tablef1.appendChild(tr);
           var tablef2 = document.getElementById("f3");
           var tr2 = document.createElement("tr");
           tr2.id = 'f3_aname';
           tr2.innerHTML = "<td>a.name</td><td>SB-R</td><td>0xbbbb</td>";
           tablef2.appendChild(tr2);
           break;
       case 4:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>f1 gets complete access to d and d becomes private. </li><li>f3 gets access to d.name.</li>";

           var tablef1 = document.getElementById("f1");
           var tr = document.createElement("tr");
           tr.id = 'f1_d';
           tr.innerHTML = "<td>d</td><td>FB</td><td>0xaaaa</td>";
           var tr2 = document.createElement("tr");
           tr2.id = 'f1_dname';
           tr2.innerHTML = "<td>d.name</td><td>FB</td><td>0xbbbb</td>";
           tablef1.appendChild(tr);          
           tablef1.appendChild(tr2);
           var tablef2 = document.getElementById("f3");
           var tr3 = document.createElement("tr");
           tr3.id = 'f3_dname';
           tr3.innerHTML = "<td>d.name</td><td>SB-R</td><td>0xbbbb</td>";
           tablef2.appendChild(tr3);
           break;
       case 5:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>f1 has no access to c. </li><li>a's private value now is equal to c's public value.</li>";

           var tr = document.getElementById('f1_aname');
           tr.remove();
           var tr = document.getElementById('f3_aname');
           tr.remove();
           var tr = document.getElementById('f1_a');
           tr.innerHTML="<td>a</td><td>FB</td><td>0xjunk</td>";
           break;
       case 6:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>f2 is a utility function. </li><li>works perfectly! since f1 has access to d and b.</li>";

           break;
       case 7:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>f2 is a utility function. </li><li>may not work properly ! since f1 has no access to c</li>";

           break;
       case 8:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>works perfectly! since f3 has access to both b and c. </li><li><b>Note:</b>f3 cannot write into these variables.</li>";

           break;
       case 9:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>May not work properly ! since f3 has no access to d. </li><li><b>Note:</b>f3 cannot write into these variables.</li>";

           break;
       case 10:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>Since f1 has no access to c, only b's properties are added to a after computation with c_public. </li><li>f3 can access this value of a.f1 retains access to a.</li>";

           var tr = document.getElementById('f1_a');
           tr.innerHTML="<td>a</td><td>FB</td><td>0xbadb</td>";
           var tablef2 = document.getElementById("f3");
           var tr3 = document.createElement("tr");
           tr3.id = 'f3_a';
           tr3.innerHTML = "<td>a</td><td>SB-R</td><td>0xbadb</td>";
           tablef2.appendChild(tr3);
           break;
       case 11:
           var explanation = document.getElementById("InFlow_example_Explanation");
           explanation.innerHTML = "<li>Since f1 has access to both b and d, an intersection of the 2 properties are added to a. </li><li>f1 retains access of a but f3 does not.</li>";

           var tr = document.getElementById('f1_a');
           tr.innerHTML="<td>a</td><td>FB</td><td>0xgood</td>";
           var tr = document.getElementById('f3_a');
           tr.remove();
           break;
       default:
           break;
   }
}
$("#jmpress").jmpress("route", ["#ASD_8", "#InFlow_1"]);
$("#jmpress").jmpress("route", ["#ASD_2_2", "#ASD_3"]);
$("#jmpress").jmpress("route", ["#ASD_5", "#ASD_8"]);
$("#jmpress").jmpress("route", ["#Rel_2", "#Rel_FlowFox_2"]);
$("#jmpress").jmpress("route", ["#Rel_FlowFox_3", "#Rel_FacetedApproach_3"]);

//function jsonpCall()
//{
//    var s = document.createElement("script");
//    s.type = "text/javascript";
//    s.id = "labtest";
//    s.src = "jsonp.js";
//    document.head.appendChild(s); 
//    //document.getElementById('labtest').remove();
//}
//function f1()
//{
//    alert('f1 called');
//}
prettyPrint();
//document.getElementById('jsonpButton').addEventListener('click',function(e){
//    jsonpCall();
//});
//document.getElementById("fsb").click();
# A Shopping trip 

- - -

The first week in a new location is always a truly different experience for every person and is special in its own way. You get to know the workings of the place by getting into many situations that often provide a good basis for comic humor. They also make for great stories that one would use for conversations throughout their life.  

- - -
 

Ramachandran's experience in Rennes was no different. As a gentle recap, Ramachandran has arrived in Rennes to start his PhD and has checked into his studio apartment near the university. This incident happened when it was yet his first week in Rennes. 

It was a Friday afternoon when Ramachandran realized his need to get many more materials to call his studio a home. Many basic amenities including a good mop needed to be purchased. Office colleagues are the most useful indeed in providing a great wealth of information and a suggestion to go to the biggest Carrefour market accessible by a bus was put forth.  

His colleagues had warned him that the hyper market would be closed on Sundays making him choose a Friday evening as a more optimal time instead.  

Ramachandran started at 17:20 from his university armed with the necessary information pertaining to the nearest bus stop and the bus number he needed to take to get to the hyper market. He had also diligently made a list of things he must buy without fail. He also carried a couple of bags since he found such a practice was highly encouraged in France; to the point of charging the consumer for bags. 

He thought about the distance of his place from everything as he walked 800 meters to the nearest bus stop. Everything had been so close in Singapore. He literally had to walk 10 steps to the nearest bus stops there ! 

He looked back at his apartment, which rested on a sort of higher plane than the rest of the surroundings and quite close to the biggest telecommunications tower in the vicinity. This tower belonged to the France Telecom, the true giant of all telecommunications in the country. It was painted in stripes of white and red as it towered to the top. The building's height was augmented with rods of protruding communications equipment that glowed their red blinks making them the most visible structure as the sun stamped its timecard to mark the end of its day. 

He took the bus as it arrived perfectly on time and took his seat. There is always a great sense of ease and satisfaction associated the simplicity of getting down at the final stop. The bus passed through a more serene looking locality with sets of beautiful houses. The gardens were not very admirable yet since the flowers had yet to make any appearance for the year. 

He got down to a sight of a huge parking lot which was mostly empty. The signage of any hyper market is hard to miss and this was going to be a long haul. He looked at his trusted watch as he entered the market. It had been his gift from his dad when he had been admitted his bachelors program. The years had passed and it had been almost nine years. Brought back to realities by the start of the engine at a nearby car, he realized it was 18:00. He had checked Carrefour's website. It was going to be open till 21:00. He had a lot of time. 

The shop itself was quite large and he was taken over that irresistible tendency to make a note of everything and how it compares in price to all past experiences. He amassed a good volume of things ranging from shaving foams to a wok. The largest item in his cart by its sheer size was the cleaning mop with the bucket. Adding in the floor cleaning liquids, disposable plates and tissues, with the various amenities like milk, fruit & bread; he was looking at around 10 kilograms of shopping. This would have been no problem had all gone according to his plan. 

He had taken his sweet time shopping and the time was now 20:20 as he finished paying. The night was truly dark now and he found himself to be the lone person waiting at the bus stop. The first ten minutes were pretty easily passed. The weather and the wind were however not the most kind of elements on that night forcing him to check the bus timings on the bus bulletin.  

It was at this moment that it hit him with unerring bewilderment … There was no bus back home, the last bus had passed through 15 minutes ago ..  

> AND HE DID NOT KNOW THE WAY BACK HOME..... 

He could not stay there either. He needed to find a route back somehow. He did not have a mobile phone number yet. His phone was an old Nokia that still portrayed GPRS as a feature ... let alone have GPS. 

The various views that piqued his interest as he came on the bus were the only true clues left for him to go back home. I would like to reiterate that the protagonist has no real French vocabulary. There was a worried look on his face considering the situation he was in. He had 3 bags full of stuff and a bucket with a mop in it. Worst, he did not even know how far away this place actually was. He recalled that the bus had taken almost 25 minutes to get there but it did stop by several places and buses are never ones to take the direct route.  

The biggest stupidity he cursed himself for was not bringing along a map of the city. He thought about his available options. He could always wave by the cars that passed along for a ride. Perhaps there would be a cab along the way. These options led him back to the bus bulletin where he realized that the bulletin had a map of the bus route. A sure-fire way to get back home would be to simply follow the bus' original route and go back home. The possibility of getting lost would also be minimal. 

Thus began the journey as he carried his several bags along a dark corridor across town in a slow walk. Thankfully, his trusty iPod was still playing. He was cold from top to bottom and temperatures were reaching around 3 degrees. His meagre jacket did nothing to save him from the rash wind. His size and baggage were not helpful in convincing any car owners to consider his predicament.  

He reached a square where the bars were still open. He tried to explain where needed to go. Alas, English was not going to be useful in this conversation. There were those who tried desperately to pass along information to him on how to reach his house but it was as well better to shout French at a wall.  

Having wasted a solid half-hour in a conversation that was bound to lead nowhere, he continued with his original plan. There was not a soul in sight as he went by residential areas of the city. It was already a solid hour and a half since he started from his original point. He had not seen a bus stop for the last 15 minutes and was wondering if he was lost.  

A few more minutes on the same direction ensued before he returned back to the previous to find he had missed a turn. This time when he did take the turn, his eyes were filled with the hopeful sight of the flickering tower. The light shone like all things eternal. There could not have been many things that could provide a relief to such a degree. 

He just continued with the original direction and found his way back home. Just before he entered his apartment, he looked at the tall majestic telecommunications tower with a huge sign of gratitude. He was also so happy that the bulletin contained the map. Had this not been the case, he would have literally been a homeless person for the night. 

He entered his house and the clock showed 23:10. A blistering hot shower and a couple of minutes in front of the stove would be last memories for the night. This night would be etched in his memories forever. 

The next morning, he had a very important task. This would be the day he went to the city center and bought his expensive smartphone. He had been a skeptic of using a touchscreen thanks to his big hands and habit of constantly dropping his phone. But the incident had changed that attitude permanently. He got his first post paid contract with a huge data connection on a phone that also had offline maps. It still had one thing from the past though. The Nokia label shone brightly. 

This had not only changed his attitude towards his phone but also broken his confidence on the city's public transport. He ended up buying his bicycle the next week and would become unfathomably attached to it. 

Thus ended the first shopping trip to the hyper market and the stories continue .... 

- - - 

> And let me make this point crystal clear - I am not Ramachandran.
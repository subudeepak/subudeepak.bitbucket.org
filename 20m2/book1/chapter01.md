# The arrival - Expectations behold 

- - - 

It was neither the best of times nor the worst of times ...  and this is not a book by Dickens. And hopefully, you have not wasted paper printing this out or money purchasing it.

- - - 

(If you are still reading, I do not mean it is a badly written piece of junk but just that it is both electronic and free to read) 
 
> And let me make this point clear - the main character in this book is not me. It is about a guy called Ramachandran, who is a close friend of mine. 

- - -

Ramachandran arrived at the Aeroport de Charles de Gaulle (CDG). It wasn’t the first time he had been here. He had been through the Airport before; albeit only on a transit. This was however the very first time he was going to step out of its confines into the nation of France.  

He stepped out of the majestic veteran Boeing 747 with a jacket he felt was thick enough for the month of march in Paris. His strides were those of immense confidence with a body which was more rounder than curvier by any measurable standards. This was an adventure that would last at least 3 years. He was here to do his PhD. 

All this would have been great had he been more familiar with French than the average tourist. Alas, his French vocabulary comprised of but two words. A good morning and a thank you weren't going to cut it for any realistic conversation. Yet, his smile hardly seemed to cater to the reality of the situation. It was the smile that only the truly ignorant can muster and it would be a smile he would never bear again on arrival to CDG.

He was excited and he reminisced about the first time he landed in Singapore. That was five years back and now he had shifted his entire life across the oceans once more. Much time had passed since and unlike the prior experience, he did not have a single personal acquaintance in France.  Yet he was happy to make this transition. He was going to have his own studio soon. Having been in Asia all his life meant that he had shared an apartment with friends for the last half a decade and had never had a place all to himself. He was quite looking forward to the huge place he was going to settle down at. 

BTW, the CDG airport was great and perhaps you could find more about it yourself from the internet. FYI- It also has PlayStations for entertainment. 

He looked back at the flight as he adjusted his baggage. The veteran 747 was making one of its final trips on this route. In a couple days, this lucrative route from Kuala Lampur to Paris would be served by the younger, and larger Airbus A380. But, like every other student making their trip in the middle of a depression, he had booked non-refundable, inflexible tickets. That being said, his interest in aircrafts were but secondary, mostly induced by his long time friend who was in every sense an airplane enthusiast.  

Ramachandran continued towards the immigration lines. This process was halted by a sudden urge to check all documents. It was a nervousness that follows anyone with the experience of losing things frequently. He noted to himself as he diligently checked his pockets for the TSA locks binding his main luggage and then the passport & finally the cash carried. Getting everything in order, his stride to the queue was again interrupted by the God of all things - a free internet kiosk. The urge to send that one mail proclaiming his arrival took over. He was in Paris, France; an exhilarating thought. 


The airport being innately warm hardly represented the reality of the weather. The protagonist stands in the immigration queue donning black jeans, a full hand shirt, a jacket only sensible for a winter in the tropics, a backpack containing his beloved Macintosh and a hand luggage filled to its maximum permissible limits.  

The immigration process itself was uneventful. He had to collect all his possessions that were encased in a single massive bag of 32 kilograms. This bag had served him well. It had already done a respectable amount of travel and been to the  United States, Italy, India, Singapore, Malaysia, Indonesia, Thailand, Cambodia and now France. He looked at his passport as it now had one more stamp from yet another destination.

The airport trains would lead him to the main railway station. Paris was white and not in any sense of racism, mind you. Seeing ice in such a setting as the amazing Paris airport for the first time meant he was filled with an emotional overload (hopefully, you bought that !!).  He would hardly see this well advertised city and be heading straight to Rennes. He was finally going to have a home of his own. A studio that was solely his.  

The rail ride to Rennes on the TGV was one that would be etched in anyone's memory. There is a nervousness that keeps the senses sharp when you re-root your entire life. It is one that can only be explained by those who have experienced it. It is a feeling of exhilarating joy and the fear of exorbitant ignorance molded together in one big rupture.  It is when you reminisce about all those people who you never cared for in the first place and think about the grandiose things to come to your life. And yet you know deep within to never keep your expectations too high. There is that urge to take a lot of pictures on your favorite camera and dance to the tunes that play on your headphones. Yet despite all this, the thoughts take over and you are left with a hyperactive brain that will keep shifting between the immense amount of information it has for no apparent reason and leaving you with an experience that is seldom forgotten.  

He would soon arrive in Rennes ...... and was lucky enough to be picked up by his professor. What began as a pure white scenery at the window in Paris was now transforming into a picturesque green as it neared Rennes.  

The only contact that he had had with his professor had been over Skype. He hardly recalled the face of the person he was to meet and was hoping his Indian origins would provide for an easily distinguishable face upon arrival.  

True to his beliefs, there was an almost instantaneous recognition as he stepped out. It is hard not to identify a person when theirs is the only face filled with a myriad of questions. The meeting must have been a shock since the student was a colossus in his physical stature when compared to the Professor. A professor's car is always identifiable in its inconspicuous appearance and precision parking with all factors such as cost, accessibility duly considered. A nervous student following behind is also easily identifiable. Thus began a cordial journey on the well-laid road.  

It is always hard to talk when you are meeting your boss for the first real time and it is equally odd for your boss when he has already let it be known that you have been hired. The first meeting always results in the same old conversation on the weather and trip details in general. It transitions to the details on the various administrative overhead to be done in the forthcoming days usually followed by a general disdain for the current system in place. The person on the receiving end comes up with often intangible solutions to non-existent problems from prior systems they have experienced. Yet, these conversations are what bring like-minded people or so to say men of the same Legion. The distance and the direction in which you are able to lead astray from the initial conversation is what determines the wavelength after all.  
 
Yet there was but one exhilarating thought that flooded Ramachandran- Today, he would have HIS SPACE. His very own studio.  
 
They arrived at the university. Ramachandran experienced his first major shock. The university, according to wikipedia was supposed to be a grand place with a huge reputation in France. Yet it appeared to be having all of 1 building which included its administrative, academic and research functionalities in 5 stories. It was a dwarf to all his experiences before.  
 
He was introduced to all his research colleagues in about 15 mins of arrival and the initial documentation and administrative overhead started. There was a big pile of documents that would make its way from the Professors table and begin a long journey through the maze that is the French bureaucracy.  
 
The most eagerly anticipated part soon followed. This was to meet the bookkeeper of the residences to get his keys to facilitate his accommodation. His professor was kind to accompany him to help decide between the three choices. Hurray ! three choices of apartments. The first one was shaped like an L and had windows surrounding the larger side of the L. The view was breathtaking as he entered. To his anguish though, it ended in a few steps. An apartment that had a kitchen, a bathroom and a bed and study with two closets was supposed to be bigger. He could not fathom how all of this was build in this small space. He checked his papers.. this space amounted to - 20 square meters. It was a sheer joke to what he thought it would be. The entire house was smaller than his room in his parents' place in India. Whatever else, it was a sheer feat of engineering on how they were able to use this small space so efficiently. But that being said, the place looked so full despite not having anything to fill the space ..  
 
20 m2 means this .. 20 square meters is just this much ...Is it not supposed to be 5 x 4 or something like that .. It never quite added up in his head as to how an apartment could be so small and why they could not just build a bigger one when all he could see out these L windows were sheer emptiness. There was a fridge below the stove !! He would not have recognized that miniature thing had it not been pointed to by his professor who was in glee.  
 
Getting back his composure, he smiled back at his boss, who went into his story of how he lived in the a similar apartment when he was doing his doctorate and how the L shape to the fridge to everything reminded him of the amazing life of a student. What else could Ramachandran do.. The apartment was nice indeed !!! 
 
The other apartments in his choices were 20 sq. Meters as well but one had allocated the space more generously to a bathroom (perhaps more fitting to someone else who wanted to dance there) and the final one had been built for someone with the kitchen as a shrine. This one had a generous pantry and the kitchen itself was a separate room with its own door and the lot -- FOR A STUDIO !!!! .. 
 
Needless to say, walking space was his priority and his professor was also delighted that the student had recognized the magnificent features of his recommendation - the L shaped room.  
 
That day hardly ended there. It was a largely eventful day which went farther. The weather was far from the warmth of the equator and his blankets were a joke. In the equator, it was hot as hell, here, spring had yet to come. It was still the last week of February.  The bookkeeper was kind enough to give him some blankets and these he would use for the rest of the time he remained in the residence. 
 
The first day in any place is directly associated with the initial shopping. This shopping is always hectic since you would need every basic necessity and you have yet to get your first salary. It is hence tradition to visit the nearest bus stop and the supermarket to know your location. It was during such an exercise that the protagonist realized the sheer isolation of the residence. The nearest bus stop was 800 meters away and the nearest shopping center a whole 1200 meters. Of course, the first day necessities were completed with the help of his car owning boss.  
 
A thought sunk in - "France was expensive …" 
 
He had arrived - and now had his very own 20 square meters in the cold corner of nowhere .. :)  

- - - 

> And let me make this point crystal clear - I am not Ramachandran. 

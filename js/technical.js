var arrayArticles = [
    {
        site:"github",
        type:"gist",
        url:"https://gist.github.com/subudeepak/9897212",
        id:"9897212"
    },
    {
        site:"github",
        type:"gist",
        url:"https://gist.github.com/subudeepak/2c63a553511a7fe2d21a",
        id:"2c63a553511a7fe2d21a"
    },
    {
        site:"onedrive",
        type:"embed",
        url:"https://onedrive.live.com/embed?cid=A104BFAC7A13EE99&resid=A104BFAC7A13EE99%21113095&authkey=AH_ZQlG8G0ld_oI&em=2"
    }
];
var githubArticle = function(data)
{
    console.log(data);
    var files = data.data.files; console.log(files);
    var filenames = Object.keys(files); console.log(filenames);
    var mdInnerHTML = "<div class='readingDescription'>"+data.data.description+"</div><br/><hr/><br/>";
    for( var i = 0 ; i < filenames.length; i++)
    {
        var thisFile = files[filenames[i]];
        mdInnerHTML += //"<div class='filename'>"+thisFile.filename+"</div>"+
                "<div class='markdownContent'>"+marked(thisFile.content)+"</div>";

    }
    document.getElementById('reading_tech').innerHTML = mdInnerHTML;
    var allCodes = document.getElementsByTagName("pre");
    for(var i = 0 ; i < allCodes.length; i++)
    {
        hljs.highlightBlock(allCodes[i]);
    }
};
//var marked = require(['js/libs/markdown/marked'],function(marked){

var currentArticle = arrayArticles[thisPage.selection.value];
switch(currentArticle.site)
{
    case "github":
       var newElement = document.createElement('script');
       newElement.src =  "https://api.github.com/gists/"+currentArticle.id+"?callback=githubArticle";
       document.head.appendChild(newElement);
       break;
    case "onedrive":
        var newElement = document.createElement('iframe');
        newElement.src = currentArticle.url;
        newElement.width = "100%";
        newElement.height = "100%";
        newElement.frameborder = "0";
        newElement.scrolling = "no";
        document.getElementById('reading_tech').innerHTML = "If this document does not load, click here:"+"<a href='"+currentArticle.url+"'> ONEDRIVE </a>";
        document.getElementById('reading_tech').appendChild(newElement);
        break;
    case "default":
        window.location = "#/error";
}
console.log("done");
//});

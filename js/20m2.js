var book20m2 = {
    arrayOf20m2Chapters: menuItems.arrayOf20m2Chapters,
    fillMd: function(){ 
                        var currentArticle = this.arrayOf20m2Chapters[thisPage.selection.value];
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', currentArticle['url'], true);
                        xhr.responseType = 'text';
                        xhr.onload = function(e) 
                                    {
                                        if (this.status === 200) 
                                        {
                                            var mdtext = this.response;
                                            var element = document.getElementById('e20m2');
                                            element.innerHTML = marked(mdtext);
                                        }
                                    };
                        xhr.send();
                       }
};
book20m2.fillMd();

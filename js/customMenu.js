/**
 * Custom Menu based on both meny as well as the multi-level push navigator.
 *
 *  Will try to make this open source once done !
 */
//I actually like how meny has been coded. It is by far cleaner. So I would like to build on meny's stuff and add it to the main part. Finally I would like to make the whole thingy an angular directive. That way, it would just work.
//MENY CODE
var menuItems =
    {
        title: "Main Menu",
	arrayOf20m2Chapters:[
		    {
			title:"Chapter 1<br/>Expectations Behold",
			site:"bitbucket",
			type:"localMd",
			url:"20m2/book1/chapter01.md",
			id:"chapter01.md"
		    },
		     {
			title:"Chapter 2<br/>A Shopping trip",
			site:"bitbucket",
			type:"localMd",
			url:"20m2/book1/chapter02.md",
			id:"chapter02.md"
		    }
	],
	arrayOfImmersionChapters:[
		    {
			title:"About",
			site:"bitbucket",
			type:"localMd",
			url:"Immersion/Book1/About.md",
			id:"About.md"
		    },
		    {
			title:"Chapter 1<br/>Immersion",
			site:"bitbucket",
			type:"localMd",
			url:"Immersion/Book1/Chapter1.md",
			id:"chapter01.md"
		    },
		     {
			title:"Chapter 2<br/>In deep waters, the Hippos will dive !! ",
			site:"bitbucket",
			type:"localMd",
			url:"Immersion/Book1/Chapter2.md",
			id:"chapter02.md"
		    }
	],
        arrayOfPoems: [
                    {
                        title:"The journey the Himalayas",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/TheJourneyThroughTheHimalayas.md",
                        id:"TheJourneyThroughTheHimalayas.md"
                    },
                    {
                        title:"The city underground",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/TheCityUnderground.md",
                        id:"TheCityUnderground.md"
                    },
                    {
                        title:"To the grown up me",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/ToTheGrownUpMe.md",
                        id:"ToTheGrownUpMe.md"
                    },
                    {
                        title:"Seeing what I see",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/SeeingWhatISee.md",
                        id:"SeeingWhatISee.md"
                    },
                    {
                        title:"Immortality Attained",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/ImmortalityAttained.md",
                        id:"ImmortalityAttained.md"
                    },
		    {
                        title:"Singara Chennai:<br/> A tale of an eternal feud",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/ATaleOfAnEternalFeud.md",
                        id:"ATaleOfAnEternalFeud.md"
                    },
		    {
                        title:"To My grandfather",
                        site:"bitbucket",
                        type:"localMd",
                        url:"poems/ToMyGrandfather.md",
                        id:"ToMyGrandfather.md"
                    }
                ],
        makeHrefs: function(aMenuArray,page){for(var i = 0 ; i < aMenuArray.length ; i++){aMenuArray[i].href = "#!/"+page+"/"+i;}}
    }
;
menuItems.makeHrefs(menuItems.arrayOfPoems,"poems"
        );
menuItems.makeHrefs(menuItems.arrayOf20m2Chapters,"20m2"
        );
menuItems.makeHrefs(menuItems.arrayOfImmersionChapters,"immersion"
        );
menuItems.children=
        [
            {title: "Technical Rant", children:[{title: "WebSockets - XSWS", href:"#!/reading/0"},{title: "Click Simulation - Why not ?", href:"#!/reading/1"}, {title: "Persona based security", href:"#!/reading/2"}], iconUrl:""},
            {title: "Resume", href:"#!/resume"},
            {title: "Personal Writing", children:[{title:"Poems",children:menuItems.arrayOfPoems},{title:"20m^2",children:menuItems.arrayOf20m2Chapters},{title:"Immersion",children:menuItems.arrayOfImmersionChapters}]},
            {title: "Music I like", href:"#!/music"},
            //{title: "Element5", href:"www.google.com"},
            //{title: "Element6", href:"www.google.com"},
            //{title: "Element7", href:"www.google.com"}
        ];
(function( root, factory ) {
    if( typeof define === 'function' && define.amd ) {
        // AMD module
        define( factory );
    } else {
        // Browser global
        root.Meny = factory();
    }
}(this, function () {

// Date.now polyfill
if( typeof Date.now !== 'function' ) Date.now = function() { return new Date().getTime(); };

	// http://coveroverflow.com/a/11381730/989439
	function mobilecheck() {
		var check = false;
		(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	}
        //
        function isWidthBigger() {

        }
	// returns the closest element to 'e' that has class "classname"
	function closest( e, classname ) {
		if( classie.has( e, classname ) ) {
			return e;
		}
		return e.parentNode && closest( e.parentNode, classname );
	}
//done with functions from mlPushMenu

var Meny = {

	// Creates a new instance of Meny
	create: function( options ) {
		return (function(){

			// Make sure the required arguments are defined
			if( !options || !options.menuElement || !options.contentsElement ) {
				throw 'You need to specify which menu and contents elements to use.';
			}

			// Make sure the menu and contents have the same parent
			if( options.menuElement.parentNode !== options.contentsElement.parentNode ) {
				throw 'The menu and contents elements must have the same parent.';
			}

			// Constants
			var POSITION_T = 'top',
				POSITION_R = 'right',
				POSITION_B = 'bottom',
				POSITION_L = 'left';

			// Feature detection for 3D transforms
			var supports3DTransforms =  'WebkitPerspective' in document.body.style ||
										'MozPerspective' in document.body.style ||
										'msPerspective' in document.body.style ||
										'OPerspective' in document.body.style ||
										'perspective' in document.body.style;
                        var currentSelectedMenu = []; //keeps note of the depth as well.
			// Default options, gets extended by passed in arguments
			var config = {
				width: 300,
				height: 300,
				position: POSITION_L,
				threshold: 40,
				angle: 30,
				overlap: 6,
				transitionDuration: '0.5s',
				transitionEasing: 'ease',
				gradient: 'rgba(0,0,0,0.20) 0%, rgba(0,0,0,0.65) 100%)',
				mouse: true,
				touch: true
			};

			// Cache references to DOM elements
			var dom = {
				menu: options.menuElement,
				contents: options.contentsElement,
				wrapper: options.menuElement.parentNode,
				cover: null,
                                menuNav: options.menuNav,
                                menuSelectors: options.menuSelectors,
                                selectedMenu: 0
			};

			// State and input
			var indentX = dom.wrapper.offsetLeft,
				indentY = dom.wrapper.offsetTop,
				touchStartX = null,
				touchStartY = null,
				touchMoveX = null,
				touchMoveY = null,
				isOpen = false,
				isMouseDown = false;

			// Precalculated transform and style states
			var menuTransformOrigin,
				menuTransformClosed,
				menuTransformOpened,
				menuStyleClosed,
				menuStyleOpened,

				contentsTransformOrigin,
				contentsTransformClosed,
				contentsTransformOpened,
				contentsStyleClosed,
				contentsStyleOpened;

			var originalStyles = {},
				addedEventListeners = [];

			// Ongoing animations (for fallback mode)
			var menuAnimation,
				contentsAnimation,
				coverAnimation;

			configure( options );

			/**
			 * Initializes Meny with the specified user options,
			 * may be called multiple times as configuration changes.
			 */
			function configure( o ) {
				// Extend the default config object with the passed in
				// options
				Meny.extend( config, o );

				setupPositions();
				setupWrapper();
				setupCover();
				setupMenu();
				setupContents();

				bindEvents();
			}

			/**
			 * Prepares the transforms for the current positioning
			 * settings.
			 */
			function setupPositions() {
				menuTransformOpened = '';
				contentsTransformClosed = '';
				menuAngle = config.angle;
				contentsAngle = config.angle / -2;

				switch( config.position ) {
					case POSITION_T:
						// Primary transform:
						menuTransformOrigin = '50% 0%';
						menuTransformClosed = 'rotateX( ' + menuAngle + 'deg ) translateY( -100% ) translateY( '+ config.overlap +'px )';
						contentsTransformOrigin = '50% 0';
						contentsTransformOpened = 'translateY( '+ config.height +'px ) rotateX( ' + contentsAngle + 'deg )';

						// Position fallback:
						menuStyleClosed = { top: '-' + (config.height-config.overlap) + 'px' };
						menuStyleOpened = { top: '0px' };
						contentsStyleClosed = { top: '0px' };
						contentsStyleOpened = { top: config.height + 'px' };
						break;

					case POSITION_R:
						// Primary transform:
						menuTransformOrigin = '100% 50%';
						menuTransformClosed = 'rotateY( ' + menuAngle + 'deg ) translateX( 100% ) translateX( -2px ) scale( 1.01 )';
						contentsTransformOrigin = '100% 50%';
						contentsTransformOpened = 'translateX( -'+ config.width +'px ) rotateY( ' + contentsAngle + 'deg )';

						// Position fallback:
						menuStyleClosed = { right: '-' + (config.width-config.overlap) + 'px' };
						menuStyleOpened = { right: '0px' };
						contentsStyleClosed = { left: '0px' };
						contentsStyleOpened = { left: '-' + config.width + 'px' };
						break;

					case POSITION_B:
						// Primary transform:
						menuTransformOrigin = '50% 100%';
						menuTransformClosed = 'rotateX( ' + -menuAngle + 'deg ) translateY( 100% ) translateY( -'+ config.overlap +'px )';
						contentsTransformOrigin = '50% 100%';
						contentsTransformOpened = 'translateY( -'+ config.height +'px ) rotateX( ' + -contentsAngle + 'deg )';

						// Position fallback:
						menuStyleClosed = { bottom: '-' + (config.height-config.overlap) + 'px' };
						menuStyleOpened = { bottom: '0px' };
						contentsStyleClosed = { top: '0px' };
						contentsStyleOpened = { top: '-' + config.height + 'px' };
						break;

					default:
						// Primary transform:
						menuTransformOrigin = '100% 50%';
						menuTransformClosed = 'translateX( -100% ) translateX( '+ config.overlap +'px ) scale( 1.01 ) rotateY( ' + -menuAngle + 'deg )';
						contentsTransformOrigin = '0 50%';
						contentsTransformOpened = 'translateX( '+ config.width +'px ) rotateY( ' + -contentsAngle + 'deg )';

						// Position fallback:
						menuStyleClosed = { left: '-' + (config.width-config.overlap) + 'px' };
						menuStyleOpened = { left: '0px' };
						contentsStyleClosed = { left: '0px' };
						contentsStyleOpened = { left: config.width + 'px' };
						break;
				}
			}

			/**
			 * The wrapper element holds the menu and contents.
			 */
			function setupWrapper() {
				// Add a class to allow for custom styles based on
				// position
				Meny.addClass( dom.wrapper, 'meny-' + config.position );

				originalStyles.wrapper = dom.wrapper.style.cssText;

				dom.wrapper.style[ Meny.prefix( 'perspective' ) ] = '800px';
				dom.wrapper.style[ Meny.prefix( 'perspectiveOrigin' ) ] = contentsTransformOrigin;
			}

			/**
			 * The cover is used to obfuscate the contents while
			 * Meny is open.
			 */
			function setupCover() {
				if( dom.cover ) {
					dom.cover.parentNode.removeChild( dom.cover );
				}

				dom.cover = document.createElement( 'div' );

				// Disabled until a falback fade in animation is added
				dom.cover.style.position = 'absolute';
				dom.cover.style.display = 'block';
				dom.cover.style.width = '100%';
				dom.cover.style.height = '100%';
				dom.cover.style.left = 0;
				dom.cover.style.top = 0;
				dom.cover.style.zIndex = 1000;
				dom.cover.style.visibility = 'hidden';
				dom.cover.style.opacity = 0;

				// Silence unimportant errors in IE8
				try {
					dom.cover.style.background = 'rgba( 0, 0, 0, 0.4 )';
					dom.cover.style.background = '-ms-linear-gradient('+ config.position +','+ config.gradient;
					dom.cover.style.background = '-moz-linear-gradient('+ config.position +','+ config.gradient;
					dom.cover.style.background = '-webkit-linear-gradient('+ config.position +','+ config.gradient;
				}
				catch( e ) {}

				if( supports3DTransforms ) {
					dom.cover.style[ Meny.prefix( 'transition' ) ] = 'all ' + config.transitionDuration +' '+ config.transitionEasing;
				}

				dom.contents.appendChild( dom.cover );
			}

			/**
			 * The meny element that folds out upon activation.
			 */
			function setupMenu() {
				// Shorthand
				var style = dom.menu.style;

				switch( config.position ) {
					case POSITION_T:
						style.width = '100%';
						style.height = config.height + 'px';
						break;

					case POSITION_R:
						style.right = '0';
						style.width = config.width + 'px';
						style.height = '100%';
						break;

					case POSITION_B:
						style.bottom = '0';
						style.width = '100%';
						style.height = config.height + 'px';
						break;

					case POSITION_L:
						style.width = config.width + 'px';
						style.height = '100%';
						break;
				}

				originalStyles.menu = style.cssText;

				style.position = 'fixed';
				style.display = 'block';
				style.zIndex = 1;

				if( supports3DTransforms ) {
					style[ Meny.prefix( 'transform' ) ] = menuTransformClosed;
					style[ Meny.prefix( 'transformOrigin' ) ] = menuTransformOrigin;
					style[ Meny.prefix( 'transition' ) ] = 'all ' + config.transitionDuration +' '+ config.transitionEasing;
				}
				else {
					Meny.extend( style, menuStyleClosed );
				}
                                openSubMenu();
			}

			/**
			 * The contents element which gets pushed aside while
			 * Meny is open.
			 */
			function setupContents() {
				// Shorthand
				var style = dom.contents.style;

				originalStyles.contents = style.cssText;

				if( supports3DTransforms ) {
					style[ Meny.prefix( 'transform' ) ] = contentsTransformClosed;
					style[ Meny.prefix( 'transformOrigin' ) ] = contentsTransformOrigin;
					style[ Meny.prefix( 'transition' ) ] = 'all ' + config.transitionDuration +' '+ config.transitionEasing;
				}
				else {
					style.position = style.position.match( /relative|absolute|fixed/gi ) ? style.position : 'relative';
					Meny.extend( style, contentsStyleClosed );
				}
			}

			/**
			 * Attaches all input event listeners.
			 */
			function bindEvents() {

				if( 'ontouchstart' in window ) {
					if( config.touch ) {
						Meny.bindEvent( document, 'touchstart', onTouchStart );
						Meny.bindEvent( document, 'touchend', onTouchEnd );
					}
					else {
						Meny.unbindEvent( document, 'touchstart', onTouchStart );
						Meny.unbindEvent( document, 'touchend', onTouchEnd );
					}
				}

				if( config.mouse ) {
					Meny.bindEvent( document, 'mousedown', onMouseDown );
					Meny.bindEvent( document, 'mouseup', onMouseUp );
					Meny.bindEvent( document, 'mousemove', onMouseMove );
				}
				else {
					Meny.unbindEvent( document, 'mousedown', onMouseDown );
					Meny.unbindEvent( document, 'mouseup', onMouseUp );
					Meny.unbindEvent( document, 'mousemove', onMouseMove );
				}
			}

			/**
			 * Expands the menu.
			 */
			function open() {

                            //TODO: mlPushMenu open
				if( !isOpen ) {
					isOpen = true;

					Meny.addClass( dom.wrapper, 'meny-active' );

					dom.cover.style.height = dom.contents.scrollHeight + 'px';
					dom.cover.style.visibility = 'visible';

					// Use transforms and transitions if available...
					if( supports3DTransforms ) {
						// 'webkitAnimationEnd oanimationend msAnimationEnd animationend transitionend'
						Meny.bindEventOnce( dom.wrapper, 'transitionend', function() {
							Meny.dispatchEvent( dom.menu, 'opened' );
						} );

						dom.cover.style.opacity = 1;

						dom.contents.style[ Meny.prefix( 'transform' ) ] = contentsTransformOpened;
						dom.menu.style[ Meny.prefix( 'transform' ) ] = menuTransformOpened;
					}
					// ...fall back on JS animation
					else {
						menuAnimation && menuAnimation.stop();
						menuAnimation = Meny.animate( dom.menu, menuStyleOpened, 500 );
						contentsAnimation && contentsAnimation.stop();
						contentsAnimation = Meny.animate( dom.contents, contentsStyleOpened, 500 );
						coverAnimation && coverAnimation.stop();
						coverAnimation = Meny.animate( dom.cover, { opacity: 1 }, 500 );
					}

					Meny.dispatchEvent( dom.menu, 'open' );
				}
			};

                        /**
                         * @param {Integer} lastSelected -The selected value
                         * @returns {undefined}
                         */
                        function openSubMenu(lastSelected)
                        {

                            //push previous to right
                            if(lastSelected == null)
                            { //need to change the whole thing to proper create element and add listener onclick instead
                                dom.menuNav.innerHTML = fillMenu(menuItems);
                                dom.menuSelectors.innerHTML = "";

                            }
                            else
                            {
                                 console.log(lastSelected);
                                var elem = document.createElement('div');
                                elem.classList.add('prevMenuItem');
                                elem.onclick = 'javascript:skipBack('+currentSelectedMenu.length+')';
                                elem.id = 'menu_prev_dynamic_id_generator_'+currentSelectedMenu.length;
                                var selectedItem = menuItems;
                                console.log(currentSelectedMenu);
                                for(i = 0 ; i < currentSelectedMenu.length ; i++)
                                {
                                    selectedItem = selectedItem.children[currentSelectedMenu[i]];
                                }
                                console.log("selected item is:");console.log(selectedItem);
                                currentSelectedMenu.push(lastSelected);
                                elem.innerHTML = selectedItem.title;
                                //dom.menuSelectors.appendChild(elem);

                                dom.menuNav.innerHTML = fillMenu(selectedItem.children[lastSelected]);
                            }
                            //create next div
                            //add to array
                        }
                        function skipBack(lvl)
                        {
                            while(currentSelectedMenu.length > lvl)
                            {
                                currentSelectedMenu.pop();
                            }
                            var selectedItem = menuItems;
                            for(i = 0 ; i < currentSelectedMenu.length ; i++)
                            {
                                selectedItem = selectedItem.children[currentSelectedMenu[i]];
                            }
                            dom.menuNav.innerHTML = fillMenu(selectedItem);
                        }
                        function fillMenu(fillArray)
                        {
                            console.log(fillArray);
                            var innerDom = "<div class='pure-menu pure-menu-open' >\
                                                    <a href=\"#\" class=\"pure-menu-heading\">"+fillArray.title+"</a><ul>";
                            if(currentSelectedMenu.length > 0)
                            {
                                innerDom += "<li class='mp-back'><a href='javascript:meny.skipBack("+(currentSelectedMenu.length-1)+")'>Back</a></li>";
                            }
                            for(i = 0; i < fillArray.children.length; i++ )
                            {
                                innerDom += "<li id='menu_dynamic_id_generator_"+i+"'><a href='javascript:"+(fillArray.children[i].href?"meny.targetSelected("+i+")":"meny.openSubMenu("+i+")")+"'>"+fillArray.children[i].title+"</a></li>";
                            }
                            innerDom += "</ul></div>";
                            return innerDom;
                        }
                        /**
                         *
                         * @param {type} selected
                         * @returns {undefined}
                         */
                       function targetSelected(selected)
                       {
                           var pre = "menu_dynamic_id_generator_";
                           var className = "pure-menu-selected";
                           var elem = document.getElementById(pre+dom.selectedMenu);
                           if(elem)
                           {
                               elem.classList.remove(className);
                           }
                           elem = document.getElementById(pre+selected);
                           if(elem)
                           {
                               elem.classList.add(className);
                           }
                           dom.selectedMenu = selected;
                           var selectedItem = menuItems;
                            for(i = 0 ; i < currentSelectedMenu.length ; i++)
                            {
                                selectedItem = selectedItem.children[currentSelectedMenu[i]];
                            }
                           document.location = selectedItem.children[selected].href;
                       }
                        /**
                         *
                         * @returns {undefined}
                         */
                        function closeSubMenu()
                        {

                        }
			/**
			 * Collapses the menu.
			 */
			function close() {

                            //ToDO: mlPushMenu close
				if( isOpen ) {
					isOpen = false;

					Meny.removeClass( dom.wrapper, 'meny-active' );

					// Use transforms and transitions if available...
					if( supports3DTransforms ) {
						// 'webkitAnimationEnd oanimationend msAnimationEnd animationend transitionend'
						Meny.bindEventOnce( dom.wrapper, 'transitionend', function() {
							Meny.dispatchEvent( dom.menu, 'closed' );
						} );

						dom.cover.style.visibility = 'hidden';
						dom.cover.style.opacity = 0;

						dom.contents.style[ Meny.prefix( 'transform' ) ] = contentsTransformClosed;
						dom.menu.style[ Meny.prefix( 'transform' ) ] = menuTransformClosed;
					}
					// ...fall back on JS animation
					else {
						menuAnimation && menuAnimation.stop();
						menuAnimation = Meny.animate( dom.menu, menuStyleClosed, 500 );
						contentsAnimation && contentsAnimation.stop();
						contentsAnimation = Meny.animate( dom.contents, contentsStyleClosed, 500 );
						coverAnimation && coverAnimation.stop();
						coverAnimation = Meny.animate( dom.cover, { opacity: 0 }, 500, function() {
							dom.cover.style.visibility = 'hidden';
							Meny.dispatchEvent( dom.menu, 'closed' );
						} );
					}
					Meny.dispatchEvent( dom.menu, 'close' );
				}
			}

			/**
			 * Unbinds Meny and resets the DOM to the state it
			 * was at before Meny was initialized.
			 */
			function destroy() {
                            //TODO : reset menu ! ?
				dom.wrapper.style.cssText = originalStyles.wrapper
				dom.menu.style.cssText = originalStyles.menu;
				dom.contents.style.cssText = originalStyles.contents;

				if( dom.cover && dom.cover.parentNode ) {
					dom.cover.parentNode.removeChild( dom.cover );
				}

				Meny.unbindEvent( document, 'touchstart', onTouchStart );
				Meny.unbindEvent( document, 'touchend', onTouchEnd );
				Meny.unbindEvent( document, 'mousedown', onMouseDown );
				Meny.unbindEvent( document, 'mouseup', onMouseUp );
				Meny.unbindEvent( document, 'mousemove', onMouseMove );

				for( var i in addedEventListeners ) {
					this.removeEventListener( addedEventListeners[i][0], addedEventListeners[i][1] );
				}

				addedEventListeners = [];
			}


			/// INPUT: /////////////////////////////////

			function onMouseDown( event ) {
				isMouseDown = true;
			}

			function onMouseMove( event ) {
				// Prevent opening/closing when mouse is down since
				// the user may be selecting text
				if( !isMouseDown ) {
					var x = event.clientX - indentX,
						y = event.clientY - indentY;

					switch( config.position ) {
						case POSITION_T:
							if( y > config.height ) {
								close();
							}
							else if( y < config.threshold ) {
								open();
							}
							break;

						case POSITION_R:
							var w = dom.wrapper.offsetWidth;
							if( x < w - config.width ) {
								close();
							}
							else if( x > w - config.threshold ) {
								open();
							}
							break;

						case POSITION_B:
							var h = dom.wrapper.offsetHeight;
							if( y < h - config.height ) {
								close();
							}
							else if( y > h - config.threshold ) {
								open();
							}
							break;

						case POSITION_L:
							if( x > config.width ) {
								close();
							}
							else if( x < config.threshold ) {
								open();
							}
							break;
					}
				}
			}

			function onMouseUp( event ) {
				isMouseDown = false;
			}

			function onTouchStart( event ) {
				touchStartX = event.touches[0].clientX - indentX;
				touchStartY = event.touches[0].clientY - indentY;
				touchMoveX = null;
				touchMoveY = null;

				Meny.bindEvent( document, 'touchmove', onTouchMove );
			}

			function onTouchMove( event ) {
				touchMoveX = event.touches[0].clientX - indentX;
				touchMoveY = event.touches[0].clientY - indentY;

				var swipeMethod = null;

				// Check for swipe gestures in any direction

				if( Math.abs( touchMoveX - touchStartX ) > Math.abs( touchMoveY - touchStartY ) ) {
					if( touchMoveX < touchStartX - config.threshold ) {
						swipeMethod = onSwipeRight;
					}
					else if( touchMoveX > touchStartX + config.threshold ) {
						swipeMethod = onSwipeLeft;
					}
				}
				else {
					if( touchMoveY < touchStartY - config.threshold ) {
						swipeMethod = onSwipeDown;
					}
					else if( touchMoveY > touchStartY + config.threshold ) {
						swipeMethod = onSwipeUp;
					}
				}

				if( swipeMethod && swipeMethod() ) {
					event.preventDefault();
				}
			}

			function onTouchEnd( event ) {
				Meny.unbindEvent( document, 'touchmove', onTouchMove );

				// If there was no movement this was a tap
				if( touchMoveX === null && touchMoveY === null ) {
					onTap();
				}
			}

			function onTap() {
				var isOverContent = ( config.position === POSITION_T && touchStartY > config.height ) ||
									( config.position === POSITION_R && touchStartX < dom.wrapper.offsetWidth - config.width ) ||
									( config.position === POSITION_B && touchStartY < dom.wrapper.offsetHeight - config.height ) ||
									( config.position === POSITION_L && touchStartX > config.width );

				if( isOverContent ) {
					close();
				}
			}

			function onSwipeLeft() {
				if( config.position === POSITION_R && isOpen ) {
					close();
					return true;
				}
				else if( config.position === POSITION_L && !isOpen ) {
					open();
					return true;
				}
			}

			function onSwipeRight() {
				if( config.position === POSITION_R && !isOpen ) {
					open();
					return true;
				}
				else if( config.position === POSITION_L && isOpen ) {
					close();
					return true;
				}
			}

			function onSwipeUp() {
				if( config.position === POSITION_B && isOpen ) {
					close();
					return true;
				}
				else if( config.position === POSITION_T && !isOpen ) {
					open();
					return true;
				}
			}

			function onSwipeDown() {
				if( config.position === POSITION_B && !isOpen ) {
					open();
					return true;
				}
				else if( config.position === POSITION_T && isOpen ) {
					close();
					return true;
				}
			}


			/// API: ///////////////////////////////////

			return {
				configure: configure,

				open: open,
				close: close,
				destroy: destroy,
                                openSubMenu: openSubMenu,
                                closeSubMenu: closeSubMenu,
                                targetSelected: targetSelected,
                                skipBack: skipBack,
				isOpen: function() {
					return isOpen;
				},

				/**
				 * Forward event binding to the menu DOM element.
				 */
				addEventListener: function( type, listener ) {
					addedEventListeners.push( [type, listener] );
					dom.menu && Meny.bindEvent( dom.menu, type, listener );
				},
				removeEventListener: function( type, listener ) {
					dom.menu && Meny.unbindEvent( dom.menu, type, listener );
				}
			};

		})();
	},

	/**
	 * Helper method, changes an element style over time.
	 */
	animate: function( element, properties, duration, callback ) {
		return (function() {
			// Will hold start/end values for all properties
			var interpolations = {};

			// Format properties
			for( var p in properties ) {
				interpolations[p] = {
					start: parseFloat( element.style[p] ) || 0,
					end: parseFloat( properties[p] ),
					unit: ( typeof properties[p] === 'string' && properties[p].match( /px|em|%/gi ) ) ? properties[p].match( /px|em|%/gi )[0] : ''
				};
			}

			var animationStartTime = Date.now(),
				animationTimeout;

			// Takes one step forward in the animation
			function step() {
				// Ease out
				var progress = 1 - Math.pow( 1 - ( ( Date.now() - animationStartTime ) / duration ), 5 );

				// Set style to interpolated value
				for( var p in interpolations ) {
					var property = interpolations[p];
					element.style[p] = property.start + ( ( property.end - property.start ) * progress ) + property.unit;
				}

				// Continue as long as we're not done
				if( progress < 1 ) {
					animationTimeout = setTimeout( step, 1000 / 60 );
				}
				else {
					callback && callback();
					stop();
				}
			}

			// Cancels the animation
			function stop() {
				clearTimeout( animationTimeout );
			}

			// Starts the animation
			step();


			/// API: ///////////////////////////////////

			return {
				stop: stop
			};
		})();
	},

	/**
	 * Extend object a with the properties of object b.
	 * If there's a conflict, object b takes precedence.
	 */
	extend: function( a, b ) {
		for( var i in b ) {
                    if( b.hasOwnProperty( i ) )
			a[ i ] = b[ i ];
		}
	},

	/**
	 * Prefixes a style property with the correct vendor.
	 */
	prefix: function( property, el ) {
		var propertyUC = property.slice( 0, 1 ).toUpperCase() + property.slice( 1 ),
			vendors = [ 'Webkit', 'Moz', 'O', 'ms' ];

		for( var i = 0, len = vendors.length; i < len; i++ ) {
			var vendor = vendors[i];

			if( typeof ( el || document.body ).style[ vendor + propertyUC ] !== 'undefined' ) {
				return vendor + propertyUC;
			}
		}

		return property;
	},

	/**
	 * Adds a class to the target element.
	 */
	addClass: function( element, name ) {
		element.className = element.className.replace( /\s+$/gi, '' ) + ' ' + name;
	},

	/**
	 * Removes a class from the target element.
	 */
	removeClass: function( element, name ) {
		element.className = element.className.replace( name, '' );
	},

	/**
	 * Adds an event listener in a browser safe way.
	 */
	bindEvent: function( element, ev, fn ) {
		if( element.addEventListener ) {
			element.addEventListener( ev, fn, false );
		}
		else {
			element.attachEvent( 'on' + ev, fn );
		}
	},

	/**
	 * Removes an event listener in a browser safe way.
	 */
	unbindEvent: function( element, ev, fn ) {
		if( element.removeEventListener ) {
			element.removeEventListener( ev, fn, false );
		}
		else {
			element.detachEvent( 'on' + ev, fn );
		}
	},

	bindEventOnce: function ( element, ev, fn ) {
		var me = this;
		var listener = function() {
			me.unbindEvent( element, ev, listener );
			fn.apply( this, arguments );
		};
		this.bindEvent( element, ev, listener );
	},

	/**
	 * Dispatches an event of the specified type from the
	 * menu DOM element.
	 */
	dispatchEvent: function( element, type, properties ) {
		if( element ) {
			var event = document.createEvent( "HTMLEvents", 1, 2 );
			event.initEvent( type, true, true );
			Meny.extend( event, properties );
			element.dispatchEvent( event );
		}
	},

	/**
	 * Retrieves query string as a key/value hash.
	 */
	getQuery: function() {
		var query = {};

		location.search.replace( /[A-Z0-9]+?=([\w|:|\/\.]*)/gi, function(a) {
			query[ a.split( '=' ).shift() ] = a.split( '=' ).pop();
		} );

		return query;
	},
        /**
         * mlPushMenu's helpers - the extend was merged with meny's helper since they are virtually the same. Meny's helper was improved
         * citing original source - taken from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
         */
        hasParent: function ( e, id ) {
		if (!e) return false;
		var el = e.target||e.srcElement||e||false;
		while (el && el.id != id) {
			el = el.parentNode||false;
		}
		return (el!==false);
	},

};

return Meny;

}));
//MLP CODE

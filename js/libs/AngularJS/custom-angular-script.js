function replaceText(str)
{
    var str1 = String(str);
    return str1.replace(/\n/g,"<br/>");
}
/*global angular */

(function (ng) {
  'use strict';
 
  var app = ng.module('ngLoadScript', []);

  app.directive('script', function() {
    return {
      restrict: 'E',
      scope: false,
      link: function(scope, elem, attr) 
      {
        if (attr.type==='text/javascript-lazy') 
        {
          var s = document.createElement("script");
          s.type = "text/javascript";                
          var src = elem.attr('src');
          if(src!==undefined)
          {
              s.src = src;
          }
          else
          {
              var code = elem.text();
              s.text = code;
          }
          document.head.appendChild(s);
          elem.remove();
          /*var f = new Function(code);
          f();*/
        }
      }
    };
  });
  app.directive('pre', function() {
    return {
        restrict: 'E',
        scope: false,
        link: function postLink(scope, element, attrs) {           
              element.html(prettyPrintOne(replaceText(element.html()),'',true));
              
        }
    };
});

}(angular));
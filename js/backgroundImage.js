var bgImg = [
    {
        imageURL: "Img/morningBurma.jpg",
        imageBy: {name:"Laxmi Sankaran", link:"https://www.linkedin.com/in/laxmii"}
    },
    {
        imageURL: "Img/southNorthKBorderBell.jpg",
        imageBy: {name:"Sumit Darak", link:"https://www.linkedin.com/in/sumitdarak"}
    },
    {
        imageURL: "Img/sheepsAtOneTreeHill.jpg",
        imageBy: {name:"Sumit Darak", link:"https://www.linkedin.com/in/sumitdarak"}
    },
    {
        imageURL: "Img/daslokRajasthan.jpg",
        imageBy: {name:"Sumit Darak", link:"https://www.linkedin.com/in/sumitdarak"}
    },
    {
        imageURL: "Img/africaElephantJoe.jpg",
        imageBy: {name:"Joe", link:"https://www.flickr.com/photos/maggie-me/"}
    }
];
var selectedBgImage = bgImg[Math.floor(Math.random() * 10) % bgImg.length];
document.getElementById('mainContents').style.backgroundImage = "url('"+selectedBgImage.imageURL+"')";
document.getElementById('mainContents').style.backgroundSize = "cover";
document.getElementById('mainContents').style.backgroundRepeat = "no-repeat";
document.getElementById('attributeBGName').innerHTML = "<a href='"+selectedBgImage.imageBy.link+"'>"+selectedBgImage.imageBy.name+"</a>";
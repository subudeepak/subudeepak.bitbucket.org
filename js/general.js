/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};
var thisPage = {value: "home", selection:{value:-1}};    
        var App = angular.module('bitbucketSite', ['ngResource','ngRoute','ngLoadScript','ngSanitize']);//'ngAnimate'
            App.config(
        ['$routeProvider', '$locationProvider',
            function ($routeProvider, $locationProvider) {

                $locationProvider.html5Mode(false).hashPrefix('!');;

                $routeProvider.when('/', {
                    templateUrl: function (routeParams){ routeParams.page = 'home'; thisPage.value = routeParams.page; thisPage.selection.value = -1; return 'pages/home.html' ;}
                    
                }); 
/*                $routeProvider.when('/script', {
                    templateUrl: '<%=server%>/pages/html/script.html'
                });*/
                 $routeProvider.when('/:page',{
                     templateUrl:function (routeParams){
                             thisPage.value = routeParams.page;
                             thisPage.selection.value=0;
//                             if(routeParams.selection){thisPage.selection.value = routeParams.selection;}                            
                             return 'pages/'+routeParams.page+'.html';
                             }
                 });
                $routeProvider.when('/:page/:selection',{
                        templateUrl: function (routeParams){
                             thisPage.value = routeParams.page;
                             thisPage.selection.value=0;
                             if(routeParams.selection){thisPage.selection.value = routeParams.selection;} 
                             return 'pages/'+routeParams.page+'.html';
                             }
                        });
                /*$routeProvider.when('/error',{
                        templateUrl: function(routeParams){routeParams.page = 'error'; return document.location.protocol + "//" +document.location.host +'/pages/error.html';}
                        
                }
                );*/
                $routeProvider.otherwise({"redirectTo":"/"});
               
            }
        ]).run(['$rootScope', '$location',function($root, $location){ $root.$on('$routeChangeError',function(){console.log("failed to change routes");$location.url('/error');});
                    $root.$on('$routeChangeSuccess',function(scope, curRoute, prevRoute) {console.log(curRoute);window.scrollTo(0,0);if(thisPage.value === 'home' || thisPage.value === 'resume' || thisPage.value === 'music' ){/*removeDiscus();*/} else{/*discus();*/}})}]);

	
function menuController($scope, $route, $routeParams)
{
    
}
/**
 * Discuss Globals
 *
var disqus_shortname = 'subudeepak'; 
var disqus_identifier = 'main';
function discus()
{ if(typeof DISQUS == 'undefined'){
    disqus_identifier = "/" + thisPage.value + (thisPage.selection.value === -1)? ("/" + (thisPage.selection.value)):'';
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();}
    else{
      disqus_identifier =  "/" + thisPage.value + (thisPage.selection.value === -1)? ("/" + (thisPage.selection.value)):'';
      DISQUS.reset({
    reload: true,
    config: function () {  
			  this.page.title = thisPage.value + "_" + (thisPage.selection.value === -1)? ("/" + (thisPage.selection.value)):'',
			  this.page.identifier = "/" + thisPage.value + (thisPage.selection.value === -1)? ("/" + (thisPage.selection.value)):'';
			  this.page.url = location.href;
			}
		    });
    }
}
function removeDiscus()
{
  document.getElementById('disqus_thread').innerHTML = '';
}*/
//var pushMenuL = new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById('mainContents') );
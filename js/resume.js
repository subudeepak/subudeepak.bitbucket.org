var resume = {
    fillMd: function(){ 
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', 'md/resume.md', true);
                        xhr.responseType = 'text';
                        xhr.onload = function(e) 
                                    {
                                        if (this.status === 200) 
                                        {
                                            var mdtext = this.response;
                                            var element = document.getElementById('resume');
                                            element.innerHTML = marked(mdtext);
                                        }
                                    };
                        xhr.send();
                       }
};
resume.fillMd();
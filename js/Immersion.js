var bookImmersion = {
    arrayOfImmersionChapters: menuItems.arrayOfImmersionChapters,
    fillMd: function(){ 
                        var currentArticle = this.arrayOfImmersionChapters[thisPage.selection.value];
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', currentArticle['url'], true);
                        xhr.responseType = 'text';
                        xhr.onload = function(e) 
                                    {
                                        if (this.status === 200) 
                                        {
                                            var mdtext = this.response;
                                            var element = document.getElementById('eImmersion');
                                            element.innerHTML = marked(mdtext);
                                        }
                                    };
                        xhr.send();
                       }
};
bookImmersion.fillMd();

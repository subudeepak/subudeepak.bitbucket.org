# [DEEPAK SUBRAMANIAN](http://subudeepak.bitbucket.org)
**Linkedin**: <http://www.linkedin.com/in/subudeepak>

**Bitbucket**: <https://bitbucket.org/subudeepak/>

**Github**: <https://gist.github.com/subudeepak>

- - -

## Certifications

[GIAC Reverse Engineering Malware \(GREM\)](https://www.youracclaim.com/badges/f9324d97-e4b2-47ac-b754-c105c90e29ad)

[GIAC Certified Forensic Analyst \(GCFA\)](https://www.youracclaim.com/badges/16b843ed-681b-4ffe-8529-a54522a613a8)

## Publications

1. **Deepak Subramanian**, G. Hiet, C. Bidan, _"A self-correcting information flow control for the web browser"_, FPS 2016, Quebec City, Canada.

16. **Deepak Subramanian**, G. Hiet, C. Bidan, _"Preventive information flow control through a mechanism of split addresses"_, ACM SIGSAC \- SIN 2016, New York, USA. Won the "Best Paper Award".

15. Willem De Groef, **Deepak Subramanian**, Martin Johns, Frank Piessens, Lieven Desmet , _"Ensuring endpoint authenticity in WebRTC peer-to-peer communication"_, ACM SEC \(at\) SAC 2016

14. Rajesh Sharma, **Deepak Subramanian**, Satish N. Srirama,_"DAPriv: Decentralized architecture for preserving the privacy of medical data"_, CoRR, 2014

13. **Deepak Subramanian**, G. Hiet, C. Bidan, _"Preventive information flow control through a mechanism of split addresses"_, SAR\-SSI 2014, Lyon France.

12. **Deepak Subramanian**, P.K.K. Loh, _"Malware Analysis for Social Networking"_. In Y. Baek, R. Ko, & T. Marsh (Eds.), Trends and Applications of Serious Gaming and Social Media, March 2014, pages: 71–87  doi:10.1007/978\-981\-4560\-26\-9_5, Springer Singapore.

11. **Deepak Subramanian**, P.K.K. Loh, _"Malware Analytics for Social Networking"_, Serious Gaming and Social Connect 2012, Singapore.

10. P.K.K. Loh, **Deepak Subramanian**, _"Fuzzy Classification Metrics for Scanner Assessment and Vulnerability Reporting"_, IEEE Transactions on Information Forensics & Security, 2010.

9.  **Deepak Subramanian**, H.T. Le, P.K.K. Loh, A.B. Premkumar, _"An Empirical Vulnerability Remediation Model"_, IEEE International Conference on Wireless Communications, Networking and Information Security (WCNIS2010), June 2010.

8.  **Deepak Subramanian**, H.T. Le, P.K.K. Loh, A.B. Premkumar, _"Quantitative Evaluation of Related Web\-based Vulnerabilities"_, The IEEE International workshop on model checking secure and reliable systems (MoCSeRS), June 2010, Singapore.

7.  **Deepak Subramanian**, H.T. Le, P.K.K. Loh, _"Assuring Quality in Vulnerability Reports for Security Risk Analysis"_, International Journal on Advances in Security, issn 1942\-2636 vol. 2, no. 2&3, year 2009,pages:226\-241.

6.  **Deepak Subramanian**, H.T. Le, P.K.K. Loh, _"Fuzzy Heuristic Design For Diagnosis Of Web\-Based Vulnerabilities"_, The IARIA & IEEE International Conference on Internet Monitoring and Protection (ICIMP), May 2009, Venice/Mestre, Italy. This paper received the "Best Paper Award" in the conference.

5.  H.T. Le, **Deepak Subramanian**, W.J. Hsu, and P.K.K. Loh, _"Scoring Web\-based Vulnerability Impact using Property\-Based Vulnerability Model"_, in The 6th International Symposium on Web and Mobile Information Services (WAMIS 2010), In conjunction with the 24th IEEE AINA Conference, April 2010, Perth, Australia.

4.  H.T. Le, **Deepak Subramanian**, W.J. Hsu, P.K.K. Loh, _"An Empirical Property\-Based Model for Vulnerability Analysis and Evaluation"_, IEEE International Workshop on Secure Service Computing (SSC), December 2009, Singapore.

3.  **Deepak Subramanian**, V.B. Kumar, _""Netphone"\-Reintegrating the net phone"_, IEEE International Conference on Cognitive Radio Oriented Wireless Networks and Communications (CrownCom), August 2007, Pages: 545\-548, Orlando, USA. This was a work\-in\-progress paper.

2.  V.B. Kumar, **Deepak Subramanian**, _""Netphone"\-Reintegrating the net phone"_, IEEE Symposium on Integrated Circuits (ISIC), September 2007, Pages: 163\-167, Singapore. This paper is an updated version of the previous paper.

1.  **Deepak Subramanian**, V.B. Kumar, _""The Device" of the Transport Layer"_, IEEE International Workshop on Information Assurance Middleware for Communications (IAMCOM), January 2007, Bangalore, India. This paper won the "Best Poster Award".

## Experience

*   Security Researcher, AXA Group Security \[September 2018 \- _Current_\]

*   Security Consultant, Trusted Labs \[March 2017 - September 2018\]

    _Responsible for performing security consulting and evaluation services for various clients based on their individual requirements. Specialized in Cyber Security and IoT services including hardware security assessments._

*   Researcher Student, CIDRE, Supelec \[2013 \- 2017\]

     _Pursued my PhD to model a practical and effective **information flow control** mechanism for the web browser. [Experimental prototype](https://bitbucket.org/subudeepak/v8wasd/branches/) of the model has been implemented on the **Chromium V8 engine**._

*   Visiting Scholar, DistriNet Group, KULeuven \[April 2014\]

	_Worked with the DistriNet group as one of the primary members to analyze the **security criterea** of the **WebRTC specification**._

*   Research Associate at Computer Security Lab, Temasek Laboratories, NTU \[August 2011 \- February 2013\]

    _Directly responsible for the research and development of a state\-of\-art **malware detection, analysis and classification engine**._

*   Technology Engineer, Security at ShowNearby Pte Ltd [August 2010 \- July 2011]

    _Responsible for guiding the **internal and web application security** at ShowNearby Pte Ltd. Was also part of the **iPhone development** team._

*   Intern, EADS Innovation Works \[June 2010 \- July 2010\]

    _Developed a methodology and prototype system for **"Early Cyber Threat Detection"**. This project was scheduled to be integrated into a much larger EADS project._

*   Research Student, Nanyang Technological University \[August 2008 \- June 2010\]

    _Was the key developer and one of the main researchers in the development of **"A knowledge\-based inference system for Software Vulnerabilities"**. The system was capable of a wide variety of assessment and analysis techniques and laid ground breaking novel approaches to further analysis of vulnerabilities._

*   Research Intern, TIFAC\-CORE, Velammal Engineering College \[August 2006 \- March 2008\]

    _Was part of the research wing funded by the Government of India. Explored various topics in networking, operating system design and VoIP. Had several research publications._

*   Student Intern, BSNL \[June 2006\]

    _Learnt about the operations and procedures at BSNL, India's Telecom giant._

## Technical skills

Skills			: Information Security, CyberSec, Information Flow Control, JavaScript Compilers, Access Control, Web Application Security, Malware Analysis, Reverse Engineering, Basic Forensic Analysis

Programming Languages	: C, C++, Java, Objective\-C, SQL

Scripting Languages	: JavaScript, Python

Technologies		: Chromium V8 JavaScript engine, OWASP ESAPI, Mozilla Zaphod

Vulnerability scanners	: HP WebInspect, IBM AppScan, eEye Retina, NTO Spider, Nikto, N\-Stalker web application scanner, Fortify 360 Software Security Assurance

Reverse Engineering	: OllyDbg, IDA Freeware, CaptureBAT, Volatility, SpiderMonkey, pdfid, origami framework

Others                  : MongoDB, Malheur,  GFI Sandbox, Apache Tomcat Application Server, Sun Glassfish server, Apache web\-server, Apache tcpmon, Subversion, GIT

## Education

*   Doctor of Philosophy \(CIDRE, SECCLOUD\), Supelec, France \(2013\-2016 \(expected\) \)
    \[Thesis \- "Multi\-level information flow monitoring"\] \- Part of the CominLabs SECCLOUD Project.

*   Advanced Computer Forensic Analysis and Incident Response, SANS Institute, October 2012.

*   Reverse\-Engineering Malware: Malware Analysis Tools and Techniques, Specialized Knowledge and Applications, SANS Institute, October 2011.

*   Master of Engineering (School of Computer Engineering), Research student with full Scholarship and Stipend for project funded by CSIT division of Ministry of Defense Singapore, Nanyang Technological University, Singapore, March 2011.
    \[Thesis \- "A knowledge\-based inference system for software vulnerabilities"\]

*   Bachelor of Engineering (Computer Science and Engineering), First Class with Distinction, Anna University, India, April 2008.
    \[Final Year Project \- "Securing Web Services using SOAP"\]
